import sys
import mysql.connector as mariadb

if len(sys.argv) == 3:

    connectionMariaDB = mariadb.connect(host='localhost', port='3306', user='', password='',
                                        database='openhab_db')
    cursor = connectionMariaDB.cursor()

    # Query NFC ID
    try:
        if sys.argv[1] == "Keypad":
            key_db = ["", ""]
            i = 0
            cursor.execute("SELECT passwd_key FROM users ")
            for passwd_key in cursor:
                # Obtain first element of tuple (passwd id)
                key_db[i] = passwd_key[0]
                i += 1

            if sys.argv[2] == key_db[0] or sys.argv[2] == key_db[1]:
                passwordCheck = True
            else:
                passwordCheck = False

            print(passwordCheck)

        elif sys.argv[1] == "NFC":
            nfc_db = ["", ""]
            i = 0
            cursor.execute("SELECT nfc_id FROM users ")
            for nfc_id in cursor:
                #Obtain first element of tuple (nfc id)
                nfc_db[i] = nfc_id[0]
                i += 1

            if sys.argv[2] == nfc_db[0] or sys.argv[2] == nfc_db[1]:
                passwordCheck = True
            else:
                passwordCheck = False

            print(passwordCheck)

        elif sys.argv[1] == "Finger":
            finger_db = ["", ""]
            i = 0
            cursor.execute("SELECT fingerprintID FROM users ")
            for fingerprintID in cursor:
                # Obtain first element of tuple (finger id)
                finger_db[i] = fingerprintID[0]
                i += 1

            if sys.argv[2] == finger_db[0] or sys.argv[2] == finger_db[1]:
                passwordCheck = True
            else:
                passwordCheck = False

            print(passwordCheck)

    except mariadb.Error as error:
        print("Error: {}".format(error))

    finally:
        cursor.close()
        connectionMariaDB.close()

elif len(sys.argv) == 2:
    print("Needs an extra argument to be executed. Try again!")
else:
    print("Needs two arguments to be executed. Try again!")

