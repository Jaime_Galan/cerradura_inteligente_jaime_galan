
-- openhabDB
-- Script SQL for creating a MariaDB database called openhabDB
-- @Author: Jaime Galan Martinez
-- @Date: 16/02/2020
-- @Version: 1.0

CREATE DATABASE `openhabDB` /*!40100 COLLATE 'utf8mb4_general_ci' */;

CREATE TABLE `Usuarios` (
	`idUser` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`nombreUsuario` VARCHAR(50) NOT NULL,
	`fechaCreacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=5
;

CREATE TABLE `ClavesTeclado` (
	`valueKey` CHAR(6) NOT NULL,
	`idUser` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`valueKey`),
	UNIQUE INDEX `idUser` (`idUser`),
	INDEX `fk_idUserKeypad` (`idUser`),
	CONSTRAINT `fk_idUserKeypad` FOREIGN KEY (`idUser`) REFERENCES `Usuarios` (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `Huellas` (
	`idFingerPrint` TINYINT(3) UNSIGNED NOT NULL,
	`idUser` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`idFingerPrint`),
	INDEX `fk_idUserFinger` (`idUser`),
	CONSTRAINT `fk_idUserFinger` FOREIGN KEY (`idUser`) REFERENCES `Usuarios` (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `NFCs` (
	`uid_NFC` CHAR(7) NOT NULL,
	`idUser` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`uid_NFC`),
	UNIQUE INDEX `idUser` (`idUser`),
	INDEX `fk_idUserNFC` (`idUser`),
	CONSTRAINT `fk_idUserNFC` FOREIGN KEY (`idUser`) REFERENCES `Usuarios` (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `RestrictionDate` (
	`Fecha` DATE NOT NULL,
	`HoraEntrada` TIME NULL DEFAULT NULL,
	`idUser` INT(10) UNSIGNED NOT NULL,
	INDEX `fk_idUserRDate` (`idUser`),
	CONSTRAINT `fk_idUserRDate` FOREIGN KEY (`idUser`) REFERENCES `Usuarios` (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `RestrictionDay` (
	`dias` CHAR(9) NOT NULL,
	`idUser` INT(10) UNSIGNED NOT NULL,
	INDEX `fk_idUserRDay` (`idUser`),
	CONSTRAINT `fk_idUserRDay` FOREIGN KEY (`idUser`) REFERENCES `Usuarios` (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `RestrictionHour` (
	`HoraEntrada` TIME NOT NULL,
	`idUser` INT(10) UNSIGNED NOT NULL,
	UNIQUE INDEX `idUser` (`idUser`),
	INDEX `fk_idUserRHour` (`idUser`),
	CONSTRAINT `fk_idUserRHour` FOREIGN KEY (`idUser`) REFERENCES `Usuarios` (`idUser`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
