"""
 queryControlData.py

 Consulta una MariaDB para comprobar si la contraseña introducida por el usuario (Teclado, NFC o Huella dactilar)
 fue correcta, y si las restricciones de fecha, día y hora se cumplen para autorizar o denegar el acceso al usuario.

 Queries a MariaDB for check if the password entered by the user (Keypad, NFC or Fingerprint) was correct
 and if date, day and hour restrictions grant or denied  the access to the user.

 Author: Jaime Galan Martinez
 Version: v1.0

"""
import sys
import mysql.connector as mariadb
import datetime


def get_current_day():
    days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    time = datetime.datetime.now()
    # Return the day of the week as an integer (Monday = 0 , Sunday = 6)
    weekday = time.weekday()
    return days[weekday]


def get_current_date():

    return datetime.date.today()


def print_message_access_door( isConcreteDay, isConcreteDayToday, isTimeGreaterEqualThanEntryTime, isDateToday,
                               day,entryHour, isTimeGreaterEqualThanEntryTime_Date, entryHourDate , isDateRestriction,
                               date, isWeekend, isWeekdays):
    if isConcreteDay:  # OK
        if isConcreteDayToday:
            if entryHour is None:
                print(userName + " access available today " + str(day))
            else:  # if entryHour:
                # Today is not the date but the user can enter this day.
                if isDateToday is None or isDateToday is False:
                    if isTimeGreaterEqualThanEntryTime:
                        print(userName + " access available today from " + str(entryHour))
                    else:
                        print(userName + " access denied. You can enter today from " + str(entryHour))

                else:  # Today is the date and is the same concreteDay.
                    # User can entry from entryHour date
                    if isDateToday:
                        # if isTimeGreaterEqualThanEntryTime_Date is None:
                        if entryHourDate is None:
                            print(userName + " access available today")
                        else:
                            if isTimeGreaterEqualThanEntryTime_Date:
                                print(userName + " access available today from " + str(entryHourDate))
                            else:
                                print(userName + " access denied. You can enter today from " +
                                      str(entryHourDate))

        else:  # Not the day available to enter. Check Restriction Date
            if isDateRestriction is None:
                print(userName + " access denied")
            elif isDateRestriction:
                if isDateToday:
                    # if  isTimeGreaterEqualThanEntryTime_Date is None:
                    if entryHourDate is None:
                        print(userName + " access available today")
                    else:
                        if isTimeGreaterEqualThanEntryTime_Date:
                            print(userName + " access available today from " + str(entryHourDate))
                        else:
                            print(userName + " access denied. You can enter today from "
                                  + str(entryHourDate))
                else:  # The date is not today
                    if entryHourDate is None:
                        print(userName + " access denied. Today is not " + str(date))
                    else:
                        pass

    else:  # Concrete Day = False
        if isWeekend is None and isWeekdays is None:  # OK
            if entryHour is None:
                print(userName + " access available everyday")
            else:
                if isDateToday is None or isDateToday is False:
                    if isTimeGreaterEqualThanEntryTime:
                        print(userName + " access available everyday from " + str(entryHour))
                    else:
                        print(userName + " access denied. You can enter everyday from " + str(entryHour))

                else:  # Today is the date and is the same concreteDay.
                    # User can entry from entryHour date
                    if isDateToday:
                        #  if isTimeGreaterEqualThanEntryTime_Date is None:
                        if entryHourDate is None:
                            print(userName + " access available today")
                        else:
                            if isTimeGreaterEqualThanEntryTime_Date:
                                print(userName + " access available today from " + str(entryHourDate))
                            else:
                                print(userName + " access denied. You can enter today from " +
                                      str(entryHourDate))

        if isWeekend and isWeekdays is None:
            if entryHour is None:
                print(userName + " access available in weekend")
            else:
                if isDateToday is None or isDateToday is False:
                    if isTimeGreaterEqualThanEntryTime:
                        print(userName + " access available in weekends from " + str(entryHour))
                    else:
                        print(userName + " access denied. You can enter in weekends from " + str(entryHour))

                else:  # Today is the date and is the same concreteDay.
                    # User can entry from entryHour date
                    if isDateToday:
                        if entryHourDate is None:
                            print(userName + " access available today")
                        else:
                            if isTimeGreaterEqualThanEntryTime_Date:
                                print(userName + " access available today from " + str(entryHourDate))
                            else:
                                print(userName + " access denied. You can enter today from " +
                                      str(entryHourDate))

        if isWeekend is False and isWeekdays is None:  # OK
            if entryHour is None:
                print(userName + " access denied")
            else:
                if isDateToday is None or isDateToday is False:
                    if isTimeGreaterEqualThanEntryTime:
                        print(userName + " access denied")
                    else:
                        print(userName + " access denied")

                else:  # Today is the date and is the same concreteDay.
                    # User can entry from entryHour date
                    if isDateToday:
                        if entryHourDate is None:
                            print(userName + " access available today")
                        else:
                            if isTimeGreaterEqualThanEntryTime_Date:
                                print(userName + " access available today from " + str(entryHourDate))
                            else:
                                print(userName + " access denied. You can enter today from " +
                                      str(entryHourDate))

        if isWeekdays and isWeekend is None:  # OK
            if entryHour is None:
                print(userName + " access available in weekdays")
            else:
                if isDateToday is None or isDateToday is False:
                    if isTimeGreaterEqualThanEntryTime:
                        print(userName + " access available in weekdays from " + str(entryHour))
                    else:
                        print(userName + " access denied. You can enter in weekdays from " + str(entryHour))

                else:  # Today is the date and is the same concreteDay.
                    # User can entry from entryHour date
                    if isDateToday:
                        if entryHourDate is None:
                            print(userName + " access available today")
                        else:
                            if isTimeGreaterEqualThanEntryTime_Date:
                                print(userName + " access available today from " + str(entryHourDate))
                            else:
                                print(userName + " access denied. You can enter today from " +
                                      str(entryHourDate))

        if isWeekdays is False and isWeekend is None:
            if entryHour is None:
                print(userName + " access denied")
            else:
                if isDateToday is None or isDateToday is False:
                    if isTimeGreaterEqualThanEntryTime:
                        print(userName + " access denied")
                    else:
                        print(userName + " access denied")

                else:  # Today is the date and is the same concreteDay.
                    # User can entry from entryHour date
                    if isDateToday:
                        if entryHourDate is None:
                            print(userName + " access available today")
                        else:
                            if isTimeGreaterEqualThanEntryTime_Date:
                                print(userName + " access available today from " + str(entryHourDate))
                            else:
                                print(userName + " access denied. You can enter today from " +
                                      str(entryHourDate))


if len(sys.argv) == 3:

    connectionMariaDB = mariadb.connect(host='localhost', port='3306', user='openhab_userDB', password='4727!db',
                                        database='openhabDB')
    cursor = connectionMariaDB.cursor()

    try:
        if sys.argv[1] == "Keypad":
            passwordKeypad = ""
            idUserKeypad = ""
            userName = ""
            passwordCheck = None

            # Day
            day = None
            isWeekend = None
            isConcreteDay = None
            isConcreteDayToday = False
            isWeekdays = None
            # EntryHour
            isTimeGreaterEqualThanEntryTime = False
            entryHour = None
            # Date
            isTimeGreaterEqualThanEntryTime_Date = False
            isDateRestriction = None
            isDateToday = None
            entryHourDate = None
            date = None

            cursor.execute("SELECT valueKey, idUser FROM openhabDB.ClavesTeclado WHERE valueKey=" + sys.argv[2])
            valueKey = cursor.fetchone()
            if valueKey is None:
                print("Not found user")
            else:
                passwordKeypad = valueKey[0]
                idUserKeypad = str(valueKey[1])

                if passwordKeypad != "":
                    cursor.execute("SELECT nombreUsuario FROM openhabDB.Usuarios WHERE idUser=" + idUserKeypad)
                    for user in cursor:
                        userName = user[0]

                    cursor.execute("SELECT dias, idUser FROM openhabDB.RestrictionDay WHERE idUser=" + idUserKeypad)
                    for dayDB in cursor:

                        if dayDB[0] is None:
                            print("No day restriction")

                        elif dayDB[0] == "Weekend":
                            isConcreteDay = False
                            if get_current_day() == "Saturday" or get_current_day() == "Sunday":
                                isWeekend = True
                            else:
                                isWeekend = False

                        elif dayDB[0] == "Weekdays":
                            isConcreteDay = False
                            if get_current_day() == "Saturday" or get_current_day() == "Sunday":
                                isWeekdays = False
                            else:
                                isWeekdays = True

                        else:
                            isConcreteDay = True
                            day = dayDB[0]
                            if get_current_day() == dayDB[0]:
                                isConcreteDayToday = True

                    cursor.execute("SELECT HoraEntrada, idUser FROM openhabDB.RestrictionHour WHERE idUser="
                                   +idUserKeypad)
                    for entryHourDB in cursor:
                        timeNow = datetime.datetime.now()
                        timeNow = datetime.timedelta(hours=timeNow.hour, minutes=timeNow.minute, seconds=timeNow.second,
                                                    microseconds=timeNow.microsecond)
                        if entryHourDB[0] is None:
                            entryHour = None
                        else:
                            entryHour = entryHourDB[0]
                            if timeNow >= entryHourDB[0]:
                                isTimeGreaterEqualThanEntryTime = True

                    cursor.execute("SELECT Fecha, HoraEntrada, idUser FROM openhabDB.RestrictionDate WHERE "
                               "idUser="+idUserKeypad)
                    for dateDB in cursor:
                        timeNow = datetime.datetime.now()
                        timeNow = datetime.timedelta(hours=timeNow.hour, minutes=timeNow.minute, seconds=timeNow.second,
                                                 microseconds=timeNow.microsecond)

                        if dateDB[0] is None:
                            isDateRestriction = False
                        else:
                            isDateRestriction = True
                            date = dateDB[0]
                            if get_current_date() == dateDB[0]:
                                isDateToday = True

                                if dateDB[1] is None:  # if entry hour is null
                                    entryHourDate = None
                                else:
                                    entryHourDate = dateDB[1]
                                    if timeNow >= dateDB[1]:
                                        isTimeGreaterEqualThanEntryTime_Date = True

                            else:
                                isDateToday = False

                    print_message_access_door(isConcreteDay, isConcreteDayToday, isTimeGreaterEqualThanEntryTime,
                                          isDateToday, day, entryHour, isTimeGreaterEqualThanEntryTime_Date,
                                          entryHourDate, isDateRestriction, date, isWeekend, isWeekdays)

                else:
                    print("Not found user")

        elif sys.argv[1] == "NFC":
            uid_NFC = ""
            idUserNFC = ""
            userName = ""
            passwordCheck = None
            # Day
            day = None
            isWeekend = None
            isConcreteDay = None
            isConcreteDayToday = False
            isWeekdays = None
            # EntryHour
            isTimeGreaterEqualThanEntryTime = False
            entryHour = None
            # Date
            isTimeGreaterEqualThanEntryTime_Date = False
            isDateRestriction = None
            isDateToday = None
            entryHourDate = None
            date = None

            cursor.execute("SELECT uid_NFC, idUser FROM openhabDB.NFCs")
            # nfc_id is a tuple containing uid_NFC and idUser
            for nfc_id in cursor:
                # Check if the uid_NFC is correct
                if sys.argv[2] == nfc_id[0]:
                    uid_NFC = nfc_id[0]
                    idUserNFC = str(nfc_id[1])
                    passwordCheck = True
                else:
                    passwordCheck = False

            if uid_NFC != "":
                cursor.execute("SELECT nombreUsuario FROM openhabDB.Usuarios WHERE idUser=" + idUserNFC)
                for user in cursor:
                    userName = user[0]

                cursor.execute("SELECT dias, idUser FROM openhabDB.RestrictionDay WHERE idUser=" + idUserNFC)
                for dayDB in cursor:

                    if dayDB[0] is None:
                        print("No day restriction")

                    elif dayDB[0] == "Weekend":
                        isConcreteDay = False
                        if get_current_day() == "Saturday" or get_current_day() == "Sunday":
                            isWeekend = True
                        else:
                            isWeekend = False

                    elif dayDB[0] == "Weekdays":
                        isConcreteDay = False
                        if get_current_day() == "Saturday" or get_current_day() == "Sunday":
                            isWeekdays = False
                        else:
                            isWeekdays = True

                    else:
                        isConcreteDay = True
                        day = dayDB[0]
                        if get_current_day() == dayDB[0]:
                            isConcreteDayToday = True

                cursor.execute("SELECT HoraEntrada, idUser FROM openhabDB.RestrictionHour WHERE idUser=" + idUserNFC)
                for entryHourDB in cursor:
                    timeNow = datetime.datetime.now()
                    timeNow = datetime.timedelta(hours=timeNow.hour, minutes=timeNow.minute, seconds=timeNow.second,
                                                 microseconds=timeNow.microsecond)
                    if entryHourDB[0] is None:
                        entryHour = None
                    else:
                        entryHour = entryHourDB[0]
                        if timeNow >= entryHourDB[0]:
                            isTimeGreaterEqualThanEntryTime = True

                cursor.execute("SELECT Fecha, HoraEntrada, idUser FROM openhabDB.RestrictionDate WHERE "
                               "idUser=" + idUserNFC)
                for dateDB in cursor:
                    timeNow = datetime.datetime.now()
                    timeNow = datetime.timedelta(hours=timeNow.hour, minutes=timeNow.minute, seconds=timeNow.second,
                                                 microseconds=timeNow.microsecond)

                    if dateDB[0] is None:
                        isDateRestriction = False
                    else:
                        isDateRestriction = True
                        date = dateDB[0]
                        if get_current_date() == dateDB[0]:
                            isDateToday = True

                            if dateDB[1] is None:  # if entry hour is null
                                entryHourDate = None
                            else:
                                entryHourDate = dateDB[1]
                                if timeNow >= dateDB[1]:
                                    isTimeGreaterEqualThanEntryTime_Date = True

                        else:
                            isDateToday = False

                print_message_access_door(isConcreteDay, isConcreteDayToday, isTimeGreaterEqualThanEntryTime,
                                          isDateToday, day, entryHour, isTimeGreaterEqualThanEntryTime_Date,
                                          entryHourDate, isDateRestriction, date, isWeekend, isWeekdays)
            else:
                print("Not found user")

        elif sys.argv[1] == "Finger":
            idFinger = ""
            idUserFinger = ""
            userName = ""
            passwordCheck = None
            # Day
            day = None
            isWeekend = None
            isConcreteDay = None
            isConcreteDayToday = False
            isWeekdays = None
            # EntryHour
            isTimeGreaterEqualThanEntryTime = False
            entryHour = None
            # Date
            isTimeGreaterEqualThanEntryTime_Date = False
            isDateRestriction = None
            isDateToday = None
            entryHourDate = None
            date = None

            cursor.execute("SELECT idFingerPrint, idUser FROM openhabDB.Huellas WHERE idFingerPrint=" + sys.argv[2])
            fingerPrintID = cursor.fetchone()
            if fingerPrintID is None:
                print("Not found user")
            else:
                idFinger = str(fingerPrintID[0])
                idUserFinger = str(fingerPrintID[1])

                if idFinger != "":
                    cursor.execute("SELECT nombreUsuario FROM openhabDB.Usuarios WHERE idUser=" + idUserFinger)
                    for user in cursor:
                        userName = user[0]

                    cursor.execute("SELECT dias, idUser FROM openhabDB.RestrictionDay WHERE idUser=" + idUserFinger)
                    for dayDB in cursor:

                        if dayDB[0] is None:
                            print("No day restriction")

                        elif dayDB[0] == "Weekend":
                            isConcreteDay = False
                            if get_current_day() == "Saturday" or get_current_day() == "Sunday":
                                isWeekend = True
                            else:
                                isWeekend = False

                        elif dayDB[0] == "Weekdays":
                            isConcreteDay = False
                            if get_current_day() == "Saturday" or get_current_day() == "Sunday":
                                isWeekdays = False
                            else:
                                isWeekdays = True

                        else:
                            isConcreteDay = True
                            day = dayDB[0]
                            if get_current_day() == dayDB[0]:
                                isConcreteDayToday = True

                    cursor.execute("SELECT HoraEntrada, idUser FROM openhabDB.RestrictionHour WHERE idUser=" + idUserFinger)
                    for entryHourDB in cursor:
                        timeNow = datetime.datetime.now()
                        timeNow = datetime.timedelta(hours=timeNow.hour, minutes=timeNow.minute, seconds=timeNow.second,
                                                 microseconds=timeNow.microsecond)
                        if entryHourDB[0] is None:
                            entryHour = None
                        else:
                            entryHour = entryHourDB[0]
                            if timeNow >= entryHourDB[0]:
                                isTimeGreaterEqualThanEntryTime = True

                    cursor.execute("SELECT Fecha, HoraEntrada, idUser FROM openhabDB.RestrictionDate WHERE "
                                "idUser=" + idUserFinger)
                    for dateDB in cursor:
                        timeNow = datetime.datetime.now()
                        timeNow = datetime.timedelta(hours=timeNow.hour, minutes=timeNow.minute, seconds=timeNow.second,
                                                    microseconds=timeNow.microsecond)

                        if dateDB[0] is None:
                            isDateRestriction = False
                        else:
                            isDateRestriction = True
                            date = dateDB[0]
                            if get_current_date() == dateDB[0]:
                                isDateToday = True

                                if dateDB[1] is None:  # if entry hour is null
                                    entryHourDate = None
                                else:
                                    entryHourDate = dateDB[1]
                                    if timeNow >= dateDB[1]:
                                        isTimeGreaterEqualThanEntryTime_Date = True

                            else:
                                isDateToday = False

                    print_message_access_door(isConcreteDay, isConcreteDayToday, isTimeGreaterEqualThanEntryTime,
                                          isDateToday, day, entryHour, isTimeGreaterEqualThanEntryTime_Date,
                                          entryHourDate, isDateRestriction, date, isWeekend, isWeekdays)

                else:
                    print("Not found user")

    except mariadb.Error as error:
        print("Error: {}".format(error))

    finally:
        cursor.close()
        connectionMariaDB.close()

elif len(sys.argv) == 2:
    print("Needs an extra argument to be executed. Try again!")
else:
    print("Needs two arguments to be executed. Try again!")

