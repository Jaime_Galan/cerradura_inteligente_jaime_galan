/*
 * module User Interface - ESP8266 (modulo Interfaz de Usuario)
 * 
 * Conecta el mUI a la red WiFi domestica, envía el SSID y la IP al MEGA y se conecta al servidor MQTT como cliente.
 * Espera a recibir la contraseña de un metodo de acceso (contraseña mediante teclado, NFC o huella) 
 * y la publica vía MQTT en el tema "home/mainDoor/passwd".
 * Tambien recibe mediante MQTT el mensaje de acceso enviado por el mControl y 
 * lo envía al MEGA mediante comunicacion serie.
 * 
 * Connects mUI to home WiFi network, sends SSID and IP data to MEGA and connects to MQTT server as a client.
 * Waits for receive a method access password (password by keypad, NFC or fingerprint)
 * and publish it via MQTT in the topic "home/mainDoor/passwd".
 * Also ESP8266 receives vía MQTT the access message sent it by mControl and sends it to MEGA using serial communication.
 * 
 * mUI_esp8266.ino
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#include "ESP8266WiFi.h"
#include <PubSubClient.h>

const char* ssid = "";
const char* password = "";

//MQTT BROKER IP - Raspberry Pi 3
const char* mqtt_server = "192.168.1.17";
const char* mqtt_user = "openhabian";
const char* mqtt_connectionPasswd = "openhabian!";
//mqtt port: TCP/IP: 1883
WiFiClient esp8266Client;
PubSubClient client(esp8266Client);

long timeLastMsg = 0;
char msg[50];
long lastReconnectAttempt = 0;


enum StatesESP{WIFI_DISCONNECTED, WIFI_CONNECTED};
StatesESP state;
uint8_t contador = 0;
bool disconnected_cmd = false;

String buffer_nfc;
String password_keypad;
String fingerID;
String fingerConfidence;

String inStringfromMEGA;
String inStringPrevious;

//Function executed when a MQTT message arrived
void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
  //topic= /home/mainDoor/accessMsg
  char msgAccessReceived [length];

  for (int i = 0; i < length; i++) {
      msgAccessReceived[i] = (char) payload[i];
  }

    serialFlush();
    Serial.print("[");
    Serial.print("accessMsg");
    Serial.print("] ");
    Serial.print(msgAccessReceived);
    Serial.print("!");
    
}

void setup() {
  Serial.begin(115200); //Communication with Mega.
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(3000);
  state = WIFI_DISCONNECTED;
 
}

void loop() {

  switch(state){

    case WIFI_DISCONNECTED:
      
      if(!disconnected_cmd){
      Serial.println("[W_DISCON]");
      }
      disconnected_cmd = true;
      serialEvente();

      //Connect to WiFi
      WiFi.begin(ssid, password);
      
      while (WiFi.status() != WL_CONNECTED) 
      {
      delay(300);
      Serial.print("*");
      }
  
      if(WiFi.status() == WL_CONNECTED){
        Serial.print("[CONN]");
        Serial.print("WiFi connection Successful");
        Serial.print("The IP Address is:");
        Serial.print(WiFi.localIP());// Print the IP address
        Serial.print(";");
        Serial.print(WiFi.SSID());
        Serial.print(";");
      
        client.setServer(mqtt_server,1883);
        client.setCallback(callback);
      
        if(!client.connected()){
          if(client.connect("mUIClient",mqtt_user, mqtt_connectionPasswd)){
            Serial.print("MQTT Connected");
          }    
        }
        if(client.subscribe("home/mainDoor/accessMsg")){
          Serial.print("Subscribed to accessMsg");
        }
        state = WIFI_CONNECTED;
      
      }
    break;

    case WIFI_CONNECTED:
    if(!client.connected()){
      long now = millis();
      if (now - lastReconnectAttempt > 3000) {
        lastReconnectAttempt = now;
        // Attempt to reconnect
        if (reconnect_MQTTClient()) {
          lastReconnectAttempt = 0;
        }
      }
      
    }else{
      client.loop();
      long now = millis();
      
      if(Serial.available()){
        serialEvente();
      }
    }
    break;

  } //state
 
} //loop

/* Function for check Serial communication with Mega 
 * and read password's data sent from MEGA to ESP8266
 * After that, ESP8266 publish password and password type via MQTT
 * in the topic "home/mainDoor/passwd"
 */
void serialEvente() {
  while (Serial.available()) {
    
    char inChar = Serial.read();
    
    inStringfromMEGA += inChar;
    if (inChar == ']') {
      if (inStringfromMEGA.indexOf("[KEYPAD]")>0) {
          inStringPrevious = "[KEYPAD]";
          password_keypad = Serial.readStringUntil('-');
          Serial.flush();
          for(uint8_t i = 0; i< 6;i++){
            Serial.print(password_keypad[i]);
          } 
          Serial.write("-");
          snprintf(msg,70,"Keypad: %s",password_keypad.c_str()); 
          Serial.print("Publish message: ");
          Serial.println(msg);
          client.publish("home/mainDoor/passwd",msg);
      }
      else if (inStringfromMEGA.indexOf("[NFC_UID]")>0) {
        inStringPrevious = "[NFC_UID]";
        buffer_nfc = Serial.readStringUntil('-');
        Serial.flush();
        for(uint8_t i = 0; i< 7;i++){
          Serial.print(buffer_nfc[i]);
        }
        Serial.write("-");
        snprintf(msg,70,"NFC ID: %s",buffer_nfc.c_str()); 
        Serial.print("Publish message: ");
        Serial.println(msg);
        client.publish("home/mainDoor/passwd",msg);
      }
      else if (inStringfromMEGA.indexOf("[FINGER_ID]")>0) {
        inStringPrevious = "[FINGER_ID]";
        fingerID = Serial.readStringUntil('-');
        Serial.flush();
        for(uint8_t i = 0; i<5;i++){
          Serial.print(fingerID[i]);
        }
        Serial.write("-");
        snprintf(msg,70,"Finger ID: %s",fingerID.c_str()); 
        Serial.print("Publish message: ");
        Serial.println(msg);
        client.publish("home/mainDoor/passwd",msg);
      
      }else if(inStringPrevious.equals(inStringfromMEGA)){

      }else {
        Serial.println("Wrong commandESP");
      }
      inStringfromMEGA = "";
    }
  }
}

//Function for reconnect MQTT Client with MQTT Server and subscribe to "home/mainDoor/accessMsg"
boolean reconnect_MQTTClient(void){
  long lastSubscribeAttempt;
  Serial.print("Attempting MQTT connection...");
  // Attempt to connect
  if (client.connect("mUIClient",mqtt_user, mqtt_connectionPasswd)) {
      
    Serial.println("MQTT connected");
    // Once connected, publish an announcement...
    client.publish("home/mainDoor/", "MQTT Connection Restored");
    //  ... and resubscribe
    long now = millis();
    if (now - lastSubscribeAttempt > 2000) {
        lastSubscribeAttempt = now;
       // Attempt to reconnect
       if(client.subscribe("home/mainDoor/accessMsg")) {
          lastSubscribeAttempt = 0;
        }
    }
 
  }
  return client.connected();
}

//Function for clear reading buffer
void serialFlush(){
  while(Serial.available() > 0){
    char t = Serial.read();
  }
}
