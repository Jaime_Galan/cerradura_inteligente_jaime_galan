#include "funciones.h"
/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * funciones.cpp
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/

 Adafruit_GFX_Button initBtn;

void show_InitiatingSystem(void){

   
    myGLCD.setFont(&FreeMono9pt7b);

    myGLCD.setCursor(85,220);
    myGLCD.setTextColor(GREEN);
    myGLCD.setTextSize(1);
    myGLCD.print("Initiating");

    myGLCD.setCursor(55,240);
    myGLCD.setTextColor(GREEN);
    myGLCD.setTextSize(1);
    myGLCD.print("SmartLock System");

    delay(800);

    myGLCD.fillScreen(TFT_BLACK);

    myGLCD.setCursor(85,220);
    myGLCD.setTextColor(GREEN);
    myGLCD.setTextSize(1);
    myGLCD.print("Please touch");

    myGLCD.setCursor(55,240);
    myGLCD.setTextColor(GREEN);
    myGLCD.setTextSize(1);
    myGLCD.print("button to continue");
  
  //INIT Button
  myGLCD.fillRoundRect(80,280, 148, 78,6,DARKGREEN);
  myGLCD.drawRoundRect(80,280, 148, 78,6,TFT_DARKGREY);
  myGLCD.setCursor(135,320);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("INIT");
  
}

void drawtopBarUI(){
    myGLCD.setFont(&FreeMono9pt7b);
    myGLCD.setCursor(4,14);
    myGLCD.setTextColor(TFT_WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("SMART LOCK");
    myGLCD.drawLine(0,20,320,20,TFT_DARKGREY);
}


void drawHomeScreen(){
    
   drawtopBarUI();

  //Interfaz UI Botones

  //BUTTON KEYBOARD
  myGLCD.fillRoundRect(X_BTN_KEYBOARD,Y_BTN_KEYBOARD, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_KEYBOARD,Y_BTN_KEYBOARD, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_KEYB,Y_TXT_KEYB);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("KEYBOARD");
  //BUTTON NFC
  myGLCD.fillRoundRect(X_BTN_NFC,Y_BTN_NFC, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_NFC,Y_BTN_NFC, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_NFC,Y_TXT_NFC); 
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("NFC");
  //BUTTON FINGERPRINT  
  myGLCD.fillRoundRect(X_BTN_FINGERPRINT,Y_BTN_FINGERPRINT, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_FINGERPRINT,Y_BTN_FINGERPRINT, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_FINGERPRINT,Y_TXT_FINGERPRINT);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("FINGERPRINT");

  //BUTTON SETTINGS
  myGLCD.fillRoundRect(X_BTN_SETTINGS,Y_BTN_SETTINGS, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_SETTINGS,Y_BTN_SETTINGS, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_SETTINGS,Y_TXT_SETTINGS); 
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("SETTINGS");

   //BUTTON INFO  
  myGLCD.fillRoundRect(X_BTN_INFO,Y_BTN_INFO, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_INFO,Y_BTN_INFO, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_INFO,Y_TXT_INFO);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("INFO");

  //BUTTON NETWORK
  myGLCD.fillRoundRect(X_BTN_NETWK,Y_BTN_NETWK, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_NETWK,Y_BTN_NETWK, BUTTONS_HOME_WIDTH, BUTTONS_HOME_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_NETWK,Y_TXT_NETWK); 
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("NETWORK");

}
  
void drawKeyboardScreen(uint8_t &positionX){

  drawtopBarUI();
  drawPasswdBoxUI(positionX);
  drawBackButtonKeyboardUI();
  drawKeyPadUI();

}

void drawNFC_Screen(){
   drawtopBarUI();
   drawBackButtonUI();

  myGLCD.fillCircle(160,240,80,RED);
  myGLCD.drawCircle(160,240,80,WHITE); 
}
void drawNFC_Check_Screen(){
   drawtopBarUI();
   drawBackButtonUI();

   myGLCD.fillCircle(160,120,70,RED);
   myGLCD.drawCircle(160,120,70,WHITE);
}
void drawFingerPrint_Screen(){
   drawtopBarUI();
   drawBackButtonUI();
   drawScanButtonUI();
   myGLCD.drawBitmap(40,100,myFingerPrintIcon,256,256,WHITE);
 
}
void drawSettings_Screen(){
   drawtopBarUI();
   drawBackButtonUI();
}
void drawInfo_Screen(){
   drawtopBarUI();
   drawInfoUI();
   drawBackButtonUI();
}
void drawNetwork_Screen(String &ssid, String &ip, boolean &receivedIP){
   drawtopBarUI();
   showWiFiDetails(ssid,ip,receivedIP);
   drawBackButtonUI();
}

void drawPasswdBoxUI(uint8_t &positionX){
  //Text Enter Password:
  myGLCD.setFont(&FreeMono9pt7b);
  myGLCD.setCursor(8,38);
  myGLCD.setTextColor(TFT_WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Enter Password:");
  //Box Password
  myGLCD.fillRoundRect(8,44, 192, 50,6,TFT_DARKGREY);
  myGLCD.drawRoundRect(8,44, 192, 50,6,TFT_WHITE);
 
  //While entering passwd
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(positionX,94);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  //Passwd length: 6 digits
  
}

void drawBackButtonKeyboardUI(){
  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_3,Y_BUTTON_BACK_KEYBOARD, BUTTON_BACK_WIDTH, BUTTON_BACK_HEIGHT,6,POWDERBLUE);
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_3,Y_BUTTON_BACK_KEYBOARD, BUTTON_BACK_WIDTH, BUTTON_BACK_HEIGHT,6,TFT_WHITE);
  
  myGLCD.fillTriangle(250,69,294,54,294,84,DARKSLATEGRAY);
  myGLCD.drawTriangle(250,69,294,54,294,84,TFT_WHITE);
}

void drawBackButtonUI(){
  myGLCD.fillRoundRect(X_BUTTON_BACK,Y_BUTTON_BACK, BUTTON_BACK_WIDTH, BUTTON_BACK_HEIGHT,6,POWDERBLUE);
  myGLCD.drawRoundRect(X_BUTTON_BACK,Y_BUTTON_BACK, BUTTON_BACK_WIDTH, BUTTON_BACK_HEIGHT,6,TFT_WHITE);
  
  myGLCD.fillTriangle(140,454,184,439,184,469,DARKSLATEGRAY);
  myGLCD.drawTriangle(140,454,184,439,184,469,TFT_WHITE);
}
void drawForwardButtonUI(){
  myGLCD.fillRoundRect(X_BUTTON_BACK,Y_BUTTON_BACK, BUTTON_BACK_WIDTH, BUTTON_BACK_HEIGHT,6,POWDERBLUE);
  myGLCD.drawRoundRect(X_BUTTON_BACK,Y_BUTTON_BACK, BUTTON_BACK_WIDTH, BUTTON_BACK_HEIGHT,6,TFT_WHITE);
  
  myGLCD.fillTriangle(140,439,140,469,184,454,DARKSLATEGRAY);
  myGLCD.drawTriangle(140,439,140,469,184,454,TFT_WHITE);
}


void writeKeyToPasswdBox(char number,uint8_t &positionX, String &password, uint8_t &currentIndexPasswd){
  
  if(password.length()<MAX_DIGITS_PASSWD){
    password.concat(number);
  
    if(currentIndexPasswd>=0 && currentIndexPasswd<6){
      myGLCD.setFont(&FreeSansBold24pt7b);
      myGLCD.setCursor(positionX,94);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("*");
      Serial.println(currentIndexPasswd);
      positionX+=26;
      if(currentIndexPasswd!=5){
            currentIndexPasswd++;
      }
    }
  }
#if DEBUG
  Serial.print("Passwd:");
  Serial.println(password);
#endif
}

void eraseKeyPasswdBox(uint8_t &positionX, String &password, uint8_t &currentIndexPasswd){
   if(password.length()<=MAX_DIGITS_PASSWD){
      if(currentIndexPasswd>=0 && currentIndexPasswd<6){

          if(positionX>20)
            positionX-=26;
          else
            positionX = 20; 

          myGLCD.setFont(&FreeSansBold24pt7b);
          myGLCD.setCursor(positionX,94);
          myGLCD.setTextColor(TFT_DARKGREY);
          myGLCD.setTextSize(1);
          myGLCD.print("*");
          if(currentIndexPasswd!=0)
            currentIndexPasswd--;
          password.remove(currentIndexPasswd);
          }
#if DEBUG
              Serial.print("Passwd_C:");
              Serial.println(password); 
#endif
  }

}

void drawKeyPadUI(){

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_1, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_1, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_1,Y_TXT_KEYS_ROW_1);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("1");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_1, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_1, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_2,Y_TXT_KEYS_ROW_1);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("2");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_1, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_1, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_3,Y_TXT_KEYS_ROW_1);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("3");
  
  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_2, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_2, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_1,Y_TXT_KEYS_ROW_2);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("4");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_2, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_2, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_2,Y_TXT_KEYS_ROW_2);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("5");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_2, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_2, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_3,Y_TXT_KEYS_ROW_2);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("6");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_3, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_3, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_1,Y_TXT_KEYS_ROW_3);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("7");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_3, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_3, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_2,Y_TXT_KEYS_ROW_3);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("8");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_3, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_3, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEYS_COL_3,Y_TXT_KEYS_ROW_3);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("9");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_4, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_1,Y_BTNS_KEYS_ROW_4, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(X_TXT_Clear,Y_TXT_Clear);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Clear");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_4, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_2,Y_BTNS_KEYS_ROW_4, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSansBold24pt7b);
  myGLCD.setCursor(X_TXT_KEY_0,Y_TXT_KEY_0);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("0");

  myGLCD.fillRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_4, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);//20 de espacio en x entre cada boton
  myGLCD.drawRoundRect(X_BTNS_KEYS_COL_3,Y_BTNS_KEYS_ROW_4, BUTTONS_KEYPAD_WIDTH, BUTTONS_KEYPAD_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(X_TXT_Send,Y_TXT_Send);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Send");
}


void drawInfoUI(){
 
 struct infoSystem info;

 info.author ="Author: Jaime Galan Martinez";
 info.systemVersion ="Version: SmartLock_v1.0";
  //Box Password
  myGLCD.fillRoundRect(8,44, 312, 50,6,ORANGE);
  myGLCD.drawRoundRect(8,44, 312, 50,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(20,69);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print(info.author);
   //Box Password
  myGLCD.fillRoundRect(8,104, 312, 50,6,ORANGE);
  myGLCD.drawRoundRect(8,104, 312, 50,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(20,129);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print(info.systemVersion);


}


// returns -1 if failed, otherwise returns ID #
int getFingerprintIDez(String msg) {
 
  uint8_t p = fingerPrnt.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = fingerPrnt.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = fingerPrnt.fingerFastSearch();
  if (p != FINGERPRINT_OK)  return -1;
  
  // found a match!
  //send fingerprint id to ESP8266
  if(msg.equals("SEND")){
    Serial3.flush();
    Serial3.print("[FINGER_ID]");
    Serial3.print(fingerPrnt.fingerID);
    Serial3.print(";"); Serial3.print(fingerPrnt.confidence);
    Serial3.print("-");
  }else if(msg.equals("CHECK")){}
 
  return fingerPrnt.fingerID; 
}

void drawScanButtonUI(){

  myGLCD.fillRoundRect(X_SCAN,Y_SCAN, SCAN_WIDTH, SCAN_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_SCAN,Y_SCAN, SCAN_WIDTH, SCAN_HEIGHT,BUTTONS_HOME_RADIUS,TFT_DARKGREY);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(X_TXT_SCAN,Y_TXT_SCAN);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("SCAN");

}

void delayKeyboard_ms(unsigned int debounce_ms){
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  unsigned long timer = millis();
  while(millis()<timer+debounce_ms){}
}

void detectPressedKeysInKeyboard(Screens &activeScreen,TSPoint p,uint8_t &key_positionX, String &passwordKeyboard, uint8_t &currentIndexPasswd, String &adminPassword, unsigned int debounce_ms, bool buttonsHomeEnabled[6],String&adminPasswdOK){
  
   if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('1',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = SETTINGS;
        }
      if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('2',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = SETTINGS;
        }
      if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('3',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = SETTINGS;
        }
      if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('4',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = SETTINGS;
        }
      if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('5',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = SETTINGS;
        }
      if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('6',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('7',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('8',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('9',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = SETTINGS;
        }
         if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          //Clear
           eraseKeyPasswdBox(key_positionX,passwordKeyboard,currentIndexPasswd);
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeKeyToPasswdBox('0',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
           delayKeyboard_ms(debounce_ms);
           //Check password (Send Button)
           if(passwordKeyboard.equals(adminPassword) && passwordKeyboard.length() == 6){
             passwordKeyboard=""; //Reset passwd for a new entry by the user next time.
             currentIndexPasswd = 0;
             key_positionX = 20;
             adminPasswdOK = "TRUE";

           }else{
             passwordKeyboard=""; //Reset passwd for a new entry by the user next time.
             currentIndexPasswd = 0;
             key_positionX = 20;
             adminPasswdOK = "FALSE";
             //Show message Wrong Passwd
           }
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK_KEYBOARD && p.y < (Y_BUTTON_BACK_KEYBOARD+BUTTON_BACK_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          passwordKeyboard=""; //Reset passwd for a new entry by the user next time.
          currentIndexPasswd = 0;
          key_positionX = 20;
          
          activeScreen = HOME;
          buttonsHomeEnabled[3]=true; //SETTINGS
        }

}

void showAccessMessage(String accessMsg){
  String firstStr="";
  String secStr="";
  String thirStr =""; 
   if(accessMsg.indexOf("avail") > 0){
      myGLCD.fillRoundRect(20,130, 282, 200,6,SEAGREEN);

    }else if (accessMsg.indexOf("denied") > 0){
      myGLCD.fillRoundRect(20,130, 282, 200,6,RED); 
    }
    else if(accessMsg.indexOf("Not found") > 0){
      myGLCD.fillRoundRect(20,130, 282, 200,6,GRAY);
    }
   myGLCD.drawRoundRect(20,130, 282, 200,6,TFT_WHITE);
   myGLCD.setFont(&FreeSans12pt7b);


  if (accessMsg.length() > 45 ){
    
    firstStr =  accessMsg.substring(0,24);
    secStr   =  accessMsg.substring(24,38);
    thirStr  =  accessMsg.substring(38, accessMsg.length());
    myGLCD.setCursor(35,180);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print(firstStr);
    myGLCD.setCursor(35,220);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print(secStr);
    myGLCD.setCursor(35,260);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print(thirStr);

  }else{
    firstStr =  accessMsg.substring(0,24);
    secStr   =  accessMsg.substring(24,accessMsg.length());
    myGLCD.setCursor(35,180);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print(firstStr);
    myGLCD.setCursor(35,220);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print(secStr);

  }
  
}

void showWiFiDetails(String &ssid, String &ip, boolean &receivedIP){
   pinMode(XM, OUTPUT);
   pinMode(YP, OUTPUT);
   myGLCD.fillRoundRect(20,130, 282, 200,6,BLUE);
   myGLCD.drawRoundRect(20,130, 282, 200,6,TFT_WHITE);
   myGLCD.setFont(&FreeSans12pt7b);
   
    if(receivedIP){
      myGLCD.setCursor(35,180);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("WiFi");
      myGLCD.setCursor(35,220);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Connected to");
      myGLCD.setCursor(35,260);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print(ssid);
      myGLCD.setCursor(35,300);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("IP Address: " + ip);
    }else{
      myGLCD.setCursor(45,220);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("WiFi disconnected");
    }
  
}




