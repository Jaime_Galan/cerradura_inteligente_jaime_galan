/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * Es el modulo encargado de la interacción del usuario con el sistema.
 * El usuario puede introducir la contraseña mediante teclado, NFC o utilizando su huella dactilar,
 * con la finalidad de abrir la cerradura y acceder al hogar. 
 * Tambien se dispone de un modo administrador al que se accede mediante contraseña por teclado.
 * En este modo el usuario puede añadir o eliminar una huella, además de comprobar los identificadores de las huellas y de las tarjetas NFCs.
 * 
 * It is the module in charge of the user's interaction with the system.
 * The user can introduce his password by means of a keyboard, NFC card or using his fingerprint, 
 * in order to open the lock and access home.
 * There's also an admin mode that can be accessed by SETTINGS option in home menu.
 * In this mode the user can add or delete a fingerprint, as well as check the fingerprint ID or NFC card ID.
 * 
 * main.cpp
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#include <Arduino.h>
#include "hardware.h"
#include "funciones.h"
#include "adminMode.h"
#define MAX_BUTTONS_HOMESCREEN 6
#define DEBOUNCE_MS 125
bool buttonEnabled = true;
bool buttonsHomeEnabled[MAX_BUTTONS_HOMESCREEN];

Screens activeScreen;
//ADMIN DELETE FINGER
String infoFinger = "NOTSET";
bool processDeleteFinger = false;
//ADMIN ADD FINGERS
bool fingerPrintsNotMatched = false;
bool processAddFinger = false;
int fingerIDSent;
bool fingerKeyMode = false;
String isFingerID_OK = "NOTSET";
uint8_t keyFinger_positionX = 20;
String fingerKeyboardID ="";
uint8_t currentIndexFingerID = 0;
//ADMIN KEYPAD
bool adminMode = false;
String adminPasswdOK = "NOTSET";
String adminPassword ="745890";
uint8_t keyAdmin_positionX = 20;
String passwordKeyboardAdmin ="";
uint8_t currentIndexAdminPasswd = 0;
//KEYPAD
uint8_t key_positionX = 20;
String passwordKeyboard="";
uint8_t currentIndexPasswd = 0;
//NFC
uint8_t uid_nfc[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint8_t uidLength;                      // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
boolean detectedNFC;
boolean card_detected = false;
//NFC CHECK
boolean detectedNFC_Check;
boolean card_detected_check = false;
//FINGERPRINT
boolean fingerPrint_Scan = false;
boolean fingerPrint_detected = false;
String inStringfromESP;
String inStringPrevious;
int fingerPrintId;
//NETWORK
boolean network_Scan = false;
String networks_Scanned= "";
boolean scanCompleted = false;
String infoIP;
String IPAddr;
String ssid;
boolean receivedIP = false;
 //ACCESS MSG
String accessMsg = "";
boolean notify_access = false;

void setup() {
   activeScreen = INIT;
  
  pinMode(LED_BUILTIN, OUTPUT);
  
  Serial.begin(115200);

  myGLCD.reset();
  myGLCD.begin(0x9486);
  myGLCD.setRotation(0);
  myGLCD.fillScreen(BLACK);
  
  for(uint8_t i = 0; i < MAX_BUTTONS_HOMESCREEN; i++){
    buttonsHomeEnabled[i] = true;
  }
  show_InitiatingSystem();

//INIT NFC
  nfc.begin();
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Set the max number of retry attempts to read from a card
  // This prevents us from waiting forever for a card, which is
  // the default behaviour of the PN532.
  nfc.setPassiveActivationRetries(0xFF);
  // configure board to read RFID tags
  nfc.SAMConfig();
  Serial.println("Waiting for an ISO14443A card");
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT); 
  myGLCD.setCursor(15,70);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("NFC Sensor detected");

  //INIT FINGERPRINT
  fingerSerial.begin(57600);
  if (fingerPrnt.verifyPassword()) {
    Serial.println("Found fingerprint sensor!");
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    myGLCD.setCursor(15,100);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Didn't find fingerprint sensor");
    while (1) { delay(1); }
  }
  fingerPrnt.getTemplateCount();
  Serial.print("Sensor contains "); Serial.print(fingerPrnt.templateCount); Serial.println(" templates");
  Serial.println("Waiting for valid finger...");
  myGLCD.setCursor(15,100);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Fingerprint Sensor detected");

 //Begin communication with ESP8266
  Serial3.begin(115200);
  myGLCD.setCursor(15,130);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Communication started");
  myGLCD.setCursor(15,160);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("with ESP8266");
}

void loop() {
  
  TSPoint p = touchScn.getPoint();  //Get touch point
   
   if(p.z > touchScn.pressureThreshhold){
      p.x = p.x + p.y;       
      p.y = p.x - p.y;            
      p.x = p.x - p.y;   
      p.x = map(p.x, TS_MINX, TS_MAXX, myGLCD.width(), 0);
      p.y = myGLCD.height()-(map(p.y, TS_MINY, TS_MAXY, myGLCD.height(), 0));
   }
   
      switch (activeScreen)
      {
       case INIT:
      
         if(p.x>80 && p.x<228 && p.y>280 && p.y<358 && buttonEnabled){
          // The user has pressed inside the  rectangle
   
          buttonEnabled = false; //Disable button
          //This is important, because the libraries are sharing pins
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);     
 
          //Erase the screen
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          activeScreen = HOME;
      
          }
        break;
       case HOME:
           //MENU HOME SCREEN
        if(p.x>X_BTN_KEYBOARD && p.x < (X_BTN_KEYBOARD+BUTTONS_HOME_WIDTH) && p.y > Y_BTN_KEYBOARD && p.y < (Y_BTN_KEYBOARD+BUTTONS_HOME_HEIGHT) && buttonsHomeEnabled[0] ){
          buttonsHomeEnabled[0]=false;
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          drawKeyboardScreen(key_positionX);
          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTN_NFC && p.x < (X_BTN_NFC+BUTTONS_HOME_WIDTH) && p.y > Y_BTN_NFC && p.y < (Y_BTN_NFC+BUTTONS_HOME_HEIGHT)  && buttonsHomeEnabled[1] ){
          buttonsHomeEnabled[1]=false;
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          drawNFC_Screen();
          activeScreen = NFC;
        }
        if(p.x>X_BTN_FINGERPRINT && p.x < (X_BTN_FINGERPRINT+BUTTONS_HOME_WIDTH) && p.y > Y_BTN_FINGERPRINT && p.y < (Y_BTN_FINGERPRINT+BUTTONS_HOME_HEIGHT)  && buttonsHomeEnabled[2]){
          buttonsHomeEnabled[2]=false;
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          drawFingerPrint_Screen();
          activeScreen = FINGERPRINT;
        }
        if(p.x>X_BTN_SETTINGS && p.x < (X_BTN_SETTINGS+BUTTONS_HOME_WIDTH) && p.y > Y_BTN_SETTINGS && p.y < (Y_BTN_SETTINGS+BUTTONS_HOME_HEIGHT) && buttonsHomeEnabled[3]){
          buttonsHomeEnabled[3]=false;
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          drawKeyboardScreen(keyAdmin_positionX);
          adminPasswdOK = "NOTSET";
          activeScreen = SETTINGS;
        }
        if(p.x>X_BTN_INFO && p.x < (X_BTN_INFO+BUTTONS_HOME_WIDTH) && p.y > Y_BTN_INFO && p.y < (Y_BTN_INFO+BUTTONS_HOME_HEIGHT)  && buttonsHomeEnabled[4]){
          buttonsHomeEnabled[4]=false;
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          drawInfo_Screen();
          activeScreen = INFO;
        }
        if(p.x>X_BTN_NETWK && p.x < (X_BTN_NETWK+BUTTONS_HOME_WIDTH) && p.y > Y_BTN_NETWK && p.y < (Y_BTN_NETWK+BUTTONS_HOME_HEIGHT)  && buttonsHomeEnabled[5]){
          buttonsHomeEnabled[5]=false;
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          drawNetwork_Screen(ssid, IPAddr, receivedIP);
          activeScreen = NETWORK;
        }
        break;

      case KEYBOARD:
       if(notify_access){
          notify_access = false;
          delayKeyboard_ms(DEBOUNCE_MS);
          showAccessMessage(accessMsg);
          delayKeyboard_ms(5000);
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          passwordKeyboard=""; //Reset passwd for a new entry by the user next time.
          currentIndexPasswd = 0;
          key_positionX = 20;
          
          activeScreen = HOME;
          buttonsHomeEnabled[0]=true;
       }
       if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('1',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = KEYBOARD;
        }
      if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('2',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = KEYBOARD;
        }
      if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('3',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = KEYBOARD;
        }
      if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('4',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = KEYBOARD;
        }
      if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('5',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = KEYBOARD;
        }
      if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('6',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('7',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          
          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('8',key_positionX,passwordKeyboard,currentIndexPasswd);
         
          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('9',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = KEYBOARD;
        }
         if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          //Clear
           eraseKeyPasswdBox(key_positionX,passwordKeyboard,currentIndexPasswd);
          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          writeKeyToPasswdBox('0',key_positionX,passwordKeyboard,currentIndexPasswd);
          
          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
           delayKeyboard_ms(DEBOUNCE_MS);
          //Send passwd(6 digits) to ESP8266
           Serial3.print("[KEYPAD]");
           for(uint8_t i = 0; i<passwordKeyboard.length();i++){

              Serial3.print(passwordKeyboard[i]);
           }
           Serial3.print("-");

          activeScreen = KEYBOARD;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK_KEYBOARD && p.y < (Y_BUTTON_BACK_KEYBOARD+BUTTON_BACK_HEIGHT)){
          delayKeyboard_ms(DEBOUNCE_MS);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          passwordKeyboard=""; //Reset passwd for a new entry by the user next time.
          currentIndexPasswd = 0;
          key_positionX = 20;
          
          activeScreen = HOME;
          buttonsHomeEnabled[0]=true;
        }
      
        break;

      case NFC:
        // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
        //   'uid' will be populated with the UID, and uidLength will indicate
        // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
       
       
         detectedNFC = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid_nfc[0], &uidLength,50);//timeout 50ms
         if(detectedNFC){
           
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillCircle(160,240,80,GREEN);
           myGLCD.drawCircle(160,240,80,WHITE);
          
           //send uid_nfc to ESP8266
           Serial3.flush();
           Serial3.print("[NFC_UID]");
           for(uint8_t i = 0; i<uidLength;i++){

                 Serial3.print(uid_nfc[i],HEX);
           }
           Serial3.print("-");
           card_detected = true;

          }else if(card_detected){
           card_detected = false;
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT); 
           myGLCD.fillCircle(160,240,80,RED);
           myGLCD.drawCircle(160,240,80,WHITE);
          
          }
           if(notify_access){
            delayKeyboard_ms(DEBOUNCE_MS);
            showAccessMessage(accessMsg);
            delayKeyboard_ms(5000);
            notify_access = false;
            myGLCD.fillScreen(TFT_BLACK);
            drawHomeScreen();
            activeScreen = HOME;
            buttonsHomeEnabled[1]=true;
          }

         //If pressed back button 
         if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          activeScreen = HOME;
          buttonsHomeEnabled[1]=true;
          
          }
        
       
        break;

      case FINGERPRINT:
         
         //If pressed scan button
         if(p.x>X_SCAN && p.x < (X_SCAN+SCAN_WIDTH) && p.y > Y_SCAN && p.y < (Y_SCAN+SCAN_HEIGHT) && !fingerPrint_Scan){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          unsigned long timer = millis();
          while(millis()<timer+DEBOUNCE_MS){}
          myGLCD.drawBitmap(40,100,myFingerPrintIcon,256,256,BLACK);
          fingerPrint_Scan = true;
         }

         if(fingerPrint_Scan && !fingerPrint_detected){

            if((fingerPrintId = getFingerprintIDez("SEND"))!= -1){
                pinMode(XM, OUTPUT);
                pinMode(YP, OUTPUT);
                myGLCD.drawBitmap(40,100,myFingerPrintWrong,256,256,BLACK);
                myGLCD.drawBitmap(40,100,myFingerPrintOK,256,256,GREEN);
                fingerPrint_detected = true;
                

            }else{
                pinMode(XM, OUTPUT);
                pinMode(YP, OUTPUT);
                myGLCD.drawBitmap(40,100,myFingerPrintWrong,256,256,RED);
                
            }
            
          }
          if(notify_access){
          notify_access = false;
          delayKeyboard_ms(DEBOUNCE_MS);
          showAccessMessage(accessMsg);
          delayKeyboard_ms(5000);
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          activeScreen = HOME;
          buttonsHomeEnabled[2]=true;
          fingerPrint_Scan = false;
          fingerPrint_detected = false;
        }
        
  
        if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          activeScreen = HOME;
          buttonsHomeEnabled[2]=true;
          fingerPrint_Scan = false;
          fingerPrint_detected = false;
        }
        break;

      case SETTINGS:

       detectPressedKeysInKeyboard(activeScreen,p,keyAdmin_positionX,passwordKeyboardAdmin,currentIndexAdminPasswd,adminPassword,DEBOUNCE_MS, buttonsHomeEnabled, adminPasswdOK);
       
       if(adminPasswdOK.equals("NOTSET")&&!adminMode){
         
       }else if(adminPasswdOK.equals("TRUE")&&!adminMode){
         adminMode = true;
         pinMode(XM, OUTPUT);
         pinMode(YP, OUTPUT);
         showMessageAccess("GRANTED");
         unsigned long timer = millis();
         while(millis()<timer+1100){}
         myGLCD.fillScreen(TFT_BLACK);
         //GO ADMIN MODE
        //Show settings admin menu
         showAdminUI();
         activeScreen = ADMIN;
          
       }else if(adminPasswdOK.equals("FALSE")&&!adminMode){
         
         pinMode(XM, OUTPUT);
         pinMode(YP, OUTPUT);
         showMessageAccess("DENIED");
         unsigned long timer = millis();
         while(millis()<timer+1100){}
         myGLCD.fillScreen(TFT_BLACK);
         adminPasswdOK = "NOTSET";
         drawKeyboardScreen(keyAdmin_positionX);
         
       }
       
        break;

      case INFO:
        if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          activeScreen = HOME;
          buttonsHomeEnabled[4]=true;
         
        }
        break;

      case NETWORK:
        
        
        if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          network_Scan = false;
          activeScreen = HOME;
          buttonsHomeEnabled[5]=true;
         
        }
        break;
      case ADMIN:
         if(p.x>X_BTN_ADD_FINGER && p.x < (X_BTN_ADD_FINGER+BTN_ADMIN_WIDTH) && p.y > Y_BTN_ADD_FINGER && p.y < (Y_BTN_ADD_FINGER+BTN_ADMIN_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillScreen(TFT_BLACK);
           drawAddFingerUI();
           activeScreen = ADD_FINGER;
         }
         if(p.x>X_BTN_DEL_FINGER && p.x < (X_BTN_DEL_FINGER+BTN_ADMIN_WIDTH) && p.y > Y_BTN_DEL_FINGER && p.y < (Y_BTN_DEL_FINGER+BTN_ADMIN_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillScreen(TFT_BLACK);
           drawDeleteFingerUI();
           activeScreen = DEL_FINGER;
         }
         if(p.x>X_BTN_CHECK_FINGER && p.x < (X_BTN_CHECK_FINGER+BTN_ADMIN_WIDTH) && p.y > Y_BTN_CHECK_FINGER && p.y < (Y_BTN_CHECK_FINGER+BTN_ADMIN_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillScreen(TFT_BLACK);
           drawFingerPrint_Screen();
           activeScreen = CHECK_FINGER;
         }
         if(p.x>X_BTN_CHECK_NFC && p.x < (X_BTN_CHECK_NFC+BTN_ADMIN_WIDTH) && p.y > Y_BTN_CHECK_NFC && p.y < (Y_BTN_CHECK_NFC+BTN_ADMIN_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillScreen(TFT_BLACK);
           drawNFC_Check_Screen();
           activeScreen = CHECK_NFC;
         }
         if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawHomeScreen();
          //saveActualStateScreen
          adminMode = false;
          activeScreen = HOME;
          buttonsHomeEnabled[3]=true;
         
        }
        break;
      case ADD_FINGER:
         if(p.x>X_BTN_KEYBOARD_FNG && p.x < (X_BTN_KEYBOARD_FNG+BTN_KEY_FNG_WIDTH) && p.y > Y_BTN_KEYBOARD_FNG && p.y < (Y_BTN_KEYBOARD_FNG+BTN_KEY_FNG_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillScreen(TFT_BLACK);
           drawKeyboardScreen(keyFinger_positionX);
           isFingerID_OK ="NOTSET";
           infoFinger ="ADD";
           activeScreen = FINGER_KEYPAD;
         }
        if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          showAdminUI();
          fingerKeyMode = false;
          infoFinger ="NOTSET";
          activeScreen = ADMIN;
         
        }
        break;
      case FINGER_KEYPAD:
        
        detectPressedKeysInKeyboardFingerID(activeScreen,p,keyFinger_positionX, fingerKeyboardID, currentIndexFingerID, fingerIDSent, DEBOUNCE_MS, isFingerID_OK);
        
        if(isFingerID_OK.equals("NOTSET")&&!fingerKeyMode){
         
        }else if(isFingerID_OK.equals("TRUE")&&!fingerKeyMode){
         fingerKeyMode = true;
         pinMode(XM, OUTPUT);
         pinMode(YP, OUTPUT);
         //Show message Access Granted
         showMessageAccess("FIN_ID_OK");
         unsigned long timer = millis();
         while(millis()<timer+1100){}
         myGLCD.fillScreen(TFT_BLACK);
         if(infoFinger.equals("ADD")){
          //GO ENROLL FINGER MODE
          drawEnrollFingerUI(fingerIDSent);
          processAddFinger = false;
          activeScreen = ENROLL_FINGER;
         }else if (infoFinger.equals("DELETE")){
            //GO DELETING FINGER MODE
            drawDeletingFingerUI(fingerIDSent);
            processDeleteFinger = false;
            activeScreen = DELETING_FINGER;

         }
          
        }else if(isFingerID_OK.equals("FALSE")&&!fingerKeyMode){
         
         pinMode(XM, OUTPUT);
         pinMode(YP, OUTPUT);
         showMessageAccess("FIN_ID_NOK");
         unsigned long timer = millis();
         while(millis()<timer+1100){}
         myGLCD.fillScreen(TFT_BLACK);
         isFingerID_OK = "NOTSET";
         drawKeyboardScreen(keyFinger_positionX);
         
        }
        break;
      case ENROLL_FINGER:
         
         if(!processAddFinger){
              getFingerprintEnroll(fingerIDSent,fingerPrintsNotMatched);
              Serial.print("ADDED FINGERPRINT ID");
              Serial.print(fingerIDSent);
         }
          if(fingerPrintsNotMatched){
            delayKeyboard_ms(400);
            myGLCD.fillRoundRect(20, 120, 282, 280,6,BLUE);
            myGLCD.drawRoundRect(20, 120, 282, 280,6,TFT_WHITE);
            processAddFinger = false;
          }else{
            processAddFinger = true;
            drawForwardButtonUI();
          }

         if(processAddFinger && p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           processAddFinger = false;
           myGLCD.fillScreen(TFT_BLACK);
           fingerKeyMode = false;
           showAdminUI();
           infoFinger ="NOTSET";
           activeScreen = ADMIN;
         }
        break;
      case DEL_FINGER:
         if(p.x>X_BTN_KEYBOARD_FNG && p.x < (X_BTN_KEYBOARD_FNG+BTN_KEY_FNG_WIDTH) && p.y > Y_BTN_KEYBOARD_FNG && p.y < (Y_BTN_KEYBOARD_FNG+BTN_KEY_FNG_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillScreen(TFT_BLACK);
           drawKeyboardScreen(keyFinger_positionX);
           isFingerID_OK ="NOTSET";
           infoFinger ="DELETE";
           activeScreen = FINGER_KEYPAD;
         }
         
        if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          showAdminUI();
          fingerKeyMode = false;
          infoFinger ="NOTSET";
          activeScreen = ADMIN;
         
        }
        break;
      case DELETING_FINGER:
         if(!processDeleteFinger){
              uint8_t result = fingerPrnt.deleteModel(fingerIDSent);
              Serial.print("DELETED FINGERPRINT ID");
              Serial.print(fingerIDSent);
              delayKeyboard_ms(30);
             if(result == FINGERPRINT_OK){
                myGLCD.setFont(&FreeSans9pt7b);
                myGLCD.setCursor(25,170);
                myGLCD.setTextColor(WHITE);
                myGLCD.setTextSize(1);
                myGLCD.print("Deleted successfully FingerID: ");
                myGLCD.print(fingerIDSent);
                processDeleteFinger = true;
                drawForwardButtonUI();
             }else if (result == FINGERPRINT_BADLOCATION){
                myGLCD.setFont(&FreeSans9pt7b);
                myGLCD.setCursor(30,170);
                myGLCD.setTextColor(WHITE);
                myGLCD.setTextSize(1);
                myGLCD.print("Location is invalid");
             }else if (result == FINGERPRINT_FLASHERR){
                myGLCD.setFont(&FreeSans9pt7b);
                myGLCD.setCursor(25,170);
                myGLCD.setTextColor(WHITE);
                myGLCD.setTextSize(1);
                myGLCD.print("Model couldn't be written to flash memory");
             }else if (result == FINGERPRINT_PACKETRECIEVEERR){
                myGLCD.setFont(&FreeSans9pt7b);
                myGLCD.setCursor(30,170);
                myGLCD.setTextColor(WHITE);
                myGLCD.setTextSize(1);
                myGLCD.print("Communication error");

             }
         }
         
        if(processDeleteFinger && p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           processDeleteFinger = false;
           myGLCD.fillScreen(TFT_BLACK);
           fingerKeyMode = false;
           infoFinger ="NOTSET";
           showAdminUI();
           activeScreen = ADMIN;
         }
        break;
      case CHECK_FINGER:

        if(p.x>X_SCAN && p.x < (X_SCAN+SCAN_WIDTH) && p.y > Y_SCAN && p.y < (Y_SCAN+SCAN_HEIGHT) && !fingerPrint_Scan){
          delayKeyboard_ms(DEBOUNCE_MS);
          myGLCD.drawBitmap(40,100,myFingerPrintIcon,256,256,BLACK);
          fingerPrint_Scan = true;
         }

         if(fingerPrint_Scan && !fingerPrint_detected){

            if((fingerPrintId = getFingerprintIDez("CHECK"))!= -1){
                pinMode(XM, OUTPUT);
                pinMode(YP, OUTPUT);
                myGLCD.drawBitmap(40,100,myFingerPrintWrong,256,256,BLACK);
                myGLCD.drawBitmap(40,100,myFingerPrintOK,256,256,GREEN);
                fingerPrint_detected = true;
                showWindowFingerID(fingerPrintId);

            }else{
                pinMode(XM, OUTPUT);
                pinMode(YP, OUTPUT);
                myGLCD.drawBitmap(40,100,myFingerPrintWrong,256,256,RED);
            }
            
          }
          if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          showAdminUI();
          activeScreen = ADMIN;
          fingerPrint_Scan = false;
          fingerPrint_detected = false;
          }
        break;
      case CHECK_NFC:
        // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
        //   'uid' will be populated with the UID, and uidLength will indicate
        // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
       
         detectedNFC_Check = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, &uid_nfc[0], &uidLength,50);//timeout 50ms
         if(detectedNFC_Check){
           
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.fillCircle(160,120,70,GREEN);
           myGLCD.drawCircle(160,120,70,WHITE);
          
           show_NFC_ID(uid_nfc,uidLength);
           card_detected_check = true;

          }else if(card_detected_check){
           card_detected_check = false;
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT); 
           myGLCD.fillCircle(160,120,70,RED);
           myGLCD.drawCircle(160,120,70,WHITE);
          
          }
           if(notify_access){
            delayKeyboard_ms(DEBOUNCE_MS);
            showAccessMessage(accessMsg);
            delayKeyboard_ms(5000);
            notify_access = false;
            myGLCD.fillScreen(TFT_BLACK);
            drawHomeScreen();
            activeScreen = HOME;
          }
          
         if(p.x>X_BUTTON_BACK && p.x < (X_BUTTON_BACK+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK && p.y < (Y_BUTTON_BACK+BUTTON_BACK_HEIGHT)){
          pinMode(XM, OUTPUT);
          pinMode(YP, OUTPUT);
          myGLCD.fillScreen(TFT_BLACK);
          showAdminUI();
          activeScreen = ADMIN;
          
          }
        
        break;
      default:
        break;
      }
      
}

/**
 * Function executed everytime  that checks communication via Serial to receive available data from ESP8266
 */
void serialEvent3() {
  while (Serial3.available()) {
    
    char inChar = Serial3.read();
    Serial.write(inChar);
    
    inStringfromESP += inChar;
    if (inChar == ']') {
      if (inStringfromESP.indexOf("[W_DISCON]")>0) {
         inStringPrevious = "[W_DISCON]";

        if(activeScreen == INIT){
           pinMode(XM, OUTPUT);
           pinMode(YP, OUTPUT);
           myGLCD.setFont(&FreeSans9pt7b);
           myGLCD.setCursor(70,Y_TXT_SCAN);
           myGLCD.setTextColor(WHITE);
           myGLCD.setTextSize(1);
           myGLCD.print("WiFi DISCONNECTED");
       }

      }else if(inStringfromESP.indexOf("[accessMsg]")> 0){
        inStringPrevious = "[accessMsg]";
        boolean pruebaS = false;
          if(inStringfromESP.startsWith("[accessMsg]")){
            pruebaS = true;
            Serial.print("p:"+pruebaS);
          }
          if(activeScreen == KEYBOARD || activeScreen == NFC || activeScreen == FINGERPRINT){
              
              accessMsg = Serial3.readStringUntil('!');
              Serial.print("RecMEG: "+accessMsg);
              notify_access = true;
          }

      }else if ( inStringfromESP.indexOf("[CONN]") > 0){
        inStringPrevious = "[CONN]";
        infoIP = Serial3.readStringUntil(';');
        IPAddr = infoIP.substring(44);
        ssid = Serial3.readStringUntil(';');
        receivedIP = true;
        Serial.print(IPAddr);
      }
      else if(inStringPrevious.equals(inStringfromESP)){

      }
      else{
        Serial.println("Wrong command");
      }
      inStringfromESP = "";
    }
  }
}