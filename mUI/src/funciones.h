/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * funciones.h
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#ifndef _FUNCIONES_H
#define _FUNCIONES_H

#include <Arduino.h>
#include "hardware.h"
#include <Fonts\FreeMono9pt7b.h>
#include <Fonts\FreeSans9pt7b.h>
#include <Fonts\FreeSans12pt7b.h>
#include <Fonts\FreeSansBold24pt7b.h>
#include "fingerprint_icons.h"

//COLORS////////////////////////////////////////////////////////////////////
#define ALICEBLUE 0xF7DF 
#define ANTIQUEWHITE 0xFF5A 
#define AQUA 0x07FF 
#define AQUAMARINE 0x7FFA 
#define AZURE 0xF7FF 
#define BEIGE 0xF7BB 
#define BISQUE 0xFF38 
#define BLACK 0x0000 
#define BLANCHEDALMOND 0xFF59 
#define BLUE 0x001F 
#define BLUEVIOLET 0x895C 
#define BROWN 0xA145 
#define BURLYWOOD 0xDDD0 
#define CADETBLUE 0x5CF4 
#define CHARTREUSE 0x7FE0 
#define CHOCOLATE 0xD343 
#define CORAL 0xFBEA 
#define CORNFLOWERBLUE 0x64BD 
#define CORNSILK 0xFFDB 
#define CRIMSON 0xD8A7 
#define CYAN 0x07FF 
#define DARKBLUE 0x0011 
#define DARKCYAN 0x0451 
#define DARKGOLDENROD 0xBC21 
#define DARKGRAY 0xAD55 
#define DARKGREEN 0x0320 
#define DARKKHAKI 0xBDAD 
#define DARKMAGENTA 0x8811 
#define DARKOLIVEGREEN 0x5345 
#define DARKORANGE 0xFC60 
#define DARKORCHID 0x9999 
#define DARKRED 0x8800 
#define DARKSALMON 0xECAF 
#define DARKSEAGREEN 0x8DF1 
#define DARKSLATEBLUE 0x49F1 
#define DARKSLATEGRAY 0x2A69 
#define DARKTURQUOISE 0x067A 
#define DARKVIOLET 0x901A 
#define DEEPPINK 0xF8B2 
#define DEEPSKYBLUE 0x05FF 
#define DIMGRAY 0x6B4D 
#define DODGERBLUE 0x1C9F 
#define FIREBRICK 0xB104 
#define FLORALWHITE 0xFFDE 
#define FORESTGREEN 0x2444 
#define FUCHSIA 0xF81F 
#define GAINSBORO 0xDEFB 
#define GHOSTWHITE 0xFFDF 
#define GOLD 0xFEA0 
#define GOLDENROD 0xDD24 
#define GRAY 0x8410 
#define GREENYELLOW 0xAFE5 
#define HONEYDEW 0xF7FE 
#define HOTPINK 0xFB56 
#define INDIANRED 0xCAEB 
#define INDIGO 0x4810 
#define IVORY 0xFFFE 
#define KHAKI 0xF731 
#define LAVENDER 0xE73F 
#define LAVENDERBLUSH 0xFF9E 
#define LAWNGREEN 0x7FE0 
#define LEMONCHIFFON 0xFFD9 
#define LIGHTBLUE 0xAEDC 
#define LIGHTCORAL 0xF410 
#define LIGHTCYAN 0xE7FF 
#define LIGHTGOLDENRODYELLOW 0xFFDA 
#define LIGHTGREEN 0x9772 
#define LIGHTGREY 0xD69A 
#define LIGHTPINK 0xFDB8 
#define LIGHTSALMON 0xFD0F 
#define LIGHTSEAGREEN 0x2595 
#define LIGHTSKYBLUE 0x867F 
#define LIGHTSLATEGRAY 0x7453 
#define LIGHTSTEELBLUE 0xB63B 
#define LIGHTYELLOW 0xFFFC 
#define LIME 0x07E0 
#define LIMEGREEN 0x3666 
#define LINEN 0xFF9C 
#define MAGENTA 0xF81F 
#define MAROON 0x8000 
#define MEDIUMAQUAMARINE 0x6675 
#define MEDIUMBLUE 0x0019 
#define MEDIUMORCHID 0xBABA 
#define MEDIUMPURPLE 0x939B 
#define MEDIUMSEAGREEN 0x3D8E 
#define MEDIUMSLATEBLUE 0x7B5D 
#define MEDIUMSPRINGGREEN 0x07D3 
#define MEDIUMTURQUOISE 0x4E99 
#define MEDIUMVIOLETRED 0xC0B0 
#define MIDNIGHTBLUE 0x18CE 
#define MINTCREAM 0xF7FF 
#define MISTYROSE 0xFF3C 
#define MOCCASIN 0xFF36 
#define NAVAJOWHITE 0xFEF5 
#define NAVY 0x0010 
#define OLDLACE 0xFFBC 
#define OLIVE 0x8400 
#define OLIVEDRAB 0x6C64 
#define ORANGE 0xFD20 
#define ORANGERED 0xFA20 
#define ORCHID 0xDB9A 
#define PALEGOLDENROD 0xEF55 
#define PALEGREEN 0x9FD3 
#define PALETURQUOISE 0xAF7D 
#define PALEVIOLETRED 0xDB92 
#define PAPAYAWHIP 0xFF7A 
#define PEACHPUFF 0xFED7 
#define PERU 0xCC27  
#define PLUM 0xDD1B 
#define POWDERBLUE 0xB71C 
#define PURPLE 0x8010 
#define RED 0xF800 
#define ROSYBROWN 0xBC71 
#define ROYALBLUE 0x435C 
#define SADDLEBROWN 0x8A22 
#define SALMON 0xFC0E 
#define SANDYBROWN 0xF52C 
#define SEAGREEN 0x2C4A 
#define SEASHELL 0xFFBD 
#define SIENNA 0xA285 
#define SILVER 0xC618 
#define SKYBLUE 0x867D 
#define SLATEBLUE 0x6AD9 
#define SLATEGRAY 0x7412 
#define SNOW 0xFFDF 
#define SPRINGGREEN 0x07EF 
#define STEELBLUE 0x4416 
#define TAN 0xD5B1 
#define TEAL 0x0410 
#define THISTLE 0xDDFB 
#define TOMATO 0xFB08 
#define TURQUOISE 0x471A 
#define VIOLET 0xEC1D 
#define WHEAT 0xF6F6 
#define WHITE 0xFFFF 
#define WHITESMOKE 0xF7BE 
#define YELLOW 0xFFE0 
#define YELLOWGREEN 0x9E66
////////////////////////////////////////////////////////////////////////////
//BUTTONS HOME SCREEN
#define BUTTONS_HOME_WIDTH 148
#define BUTTONS_HOME_HEIGHT 78
#define BUTTONS_HOME_RADIUS 6
//Keyboard Button
#define X_BTN_KEYBOARD 2
#define Y_BTN_KEYBOARD 24
#define X_TXT_KEYB 29
#define Y_TXT_KEYB 67
//NFC Button
#define X_BTN_NFC 168
#define Y_BTN_NFC  24
#define X_TXT_NFC 225
#define Y_TXT_NFC 67
//Fingerprint Button
#define X_BTN_FINGERPRINT 2
#define Y_BTN_FINGERPRINT 120
#define X_TXT_FINGERPRINT 18
#define Y_TXT_FINGERPRINT 160
//Settings Button
#define X_BTN_SETTINGS 168
#define Y_BTN_SETTINGS 120
#define X_TXT_SETTINGS 200
#define Y_TXT_SETTINGS 160
//Info Button
#define X_BTN_INFO 2
#define Y_BTN_INFO 216
#define X_TXT_INFO 55
#define Y_TXT_INFO 257
//Network Button
#define X_BTN_NETWK 168
#define Y_BTN_NETWK 216
#define X_TXT_NETWK 200
#define Y_TXT_NETWK 257
////////////////////////////////////////////////////////////////////////////
//Buttons keypad
#define BUTTONS_KEYPAD_WIDTH  80
#define BUTTONS_KEYPAD_HEIGHT 80
//Rows keypad
#define Y_BTNS_KEYS_ROW_1 110
#define Y_BTNS_KEYS_ROW_2 206
#define Y_BTNS_KEYS_ROW_3 302
#define Y_BTNS_KEYS_ROW_4 398

#define Y_TXT_KEYS_ROW_1 165
#define Y_TXT_KEYS_ROW_2 261
#define Y_TXT_KEYS_ROW_3 357

//Cols keypad
#define X_BTNS_KEYS_COL_1 8
#define X_BTNS_KEYS_COL_2 120
#define X_BTNS_KEYS_COL_3 232

#define X_TXT_KEYS_COL_1 38
#define X_TXT_KEYS_COL_2 150
#define X_TXT_KEYS_COL_3 262
//BUTTON BACK
#define Y_BUTTON_BACK_KEYBOARD 44
#define X_BUTTON_BACK 122
#define Y_BUTTON_BACK 429
#define BUTTON_BACK_WIDTH 80
#define BUTTON_BACK_HEIGHT 50
//BUTTON Clear
#define X_TXT_Clear 24
#define Y_TXT_Clear 443
//BUTTON KEY 0
#define X_TXT_KEY_0 150
#define Y_TXT_KEY_0 452
//BUTTON Send
#define X_TXT_Send 248
#define Y_TXT_Send 443
//////////////////////////////////////////////////////////////////////
#define X_TXT_SCAN 138
#define Y_TXT_SCAN  67
#define X_SCAN 98
#define Y_SCAN  32
#define SCAN_WIDTH 128
#define SCAN_HEIGHT  58
///////////////////////////////////////////////////////////////
#define MAX_DIGITS_PASSWD 6
#define DEBUG 1

/**
 * Shows the message initiating the system
 */
void show_InitiatingSystem();

/**
 * Draws the top bar UI 
 */
void drawtopBarUI();

/**
 * Draws Home Screen Menu
 */
void drawHomeScreen();

/**
 * Draws the Keyboard screen UI
 * @param positionX uint8_t &
 */
void drawKeyboardScreen(uint8_t &positionX);

/**
 * Draws the NFC Screen UI
 */
void drawNFC_Screen();

/**
 * Draws the NFC Check Screen UI
 */
void drawNFC_Check_Screen();

/**
 * Draws the Fingerprint Screen UI
 */
void drawFingerPrint_Screen();

/**
 * Draws topBarUI and back button UI
 */
void drawSettings_Screen();

/**
 * Draws top bar UI, Info UI and back button UI
 */
void drawInfo_Screen();

/**
 *  Draws the network screen, showing if it's connected or not to a WiFi network
 * @param ssid - String &
 * @param ip - String &
 * @param receivedIP - boolean &
 * 
 */
void drawNetwork_Screen(String &ssid, String &ip, boolean &receivedIP);

/**
 * Write a digit using the keyboard for entering the desired keypad password.
 * @param number - char
 * @param positionX - uint8_t &
 * @param password - String &
 * @param currentIndexPasswd - uint8_t &
 */
void writeKeyToPasswdBox(char number,uint8_t &positionX, String &password, uint8_t &currentIndexPasswd);

/**
 * Erases last digit typed using the keyboard for entering the desired keypad password
 * @param positionX -uint8_t &
 * @param password -String &
 * @param currentIndexPasswd -uint8_t &
 */
void eraseKeyPasswdBox(uint8_t &positionX, String &password, uint8_t &currentIndexPasswd);

/**
 * Draws the password box UI
 * @param positionX - uint8_t &
 */
void drawPasswdBoxUI(uint8_t &positionX);

/**
 *  Draws back button UI specified for keyboard
 */
void drawBackButtonKeyboardUI();

/**
 * Draws Back button UI
 */
void drawBackButtonUI();

/**
 *  Draws Forward Button UI
 */
void drawForwardButtonUI();

/**
 * Draws the keypad UI
 */
void drawKeyPadUI();

/**
 *  Draws Info UI displaying Author and system version.
 */
void drawInfoUI();

/**
 *  Get Fingerprint ID if msg is "CHECK". Otherwise if msg is "SEND" get the fingerprint ID  and its confidence and send it to ESP8266
 *  @param msg - String
 *  @returns -1 if failed, otherwise returns ID # 
 */
int getFingerprintIDez(String msg);

/**
 *  Draw Scan button UI
 */
void drawScanButtonUI();

/**
 * Keyboard delay specified in ms.
 * @param debounce_ms - unsigned int
 */
void delayKeyboard_ms(unsigned int debounce_ms);


enum Screens{INIT,HOME,KEYBOARD,NFC,FINGERPRINT,SETTINGS,INFO,NETWORK, ADMIN, ADD_FINGER, FINGER_KEYPAD, ENROLL_FINGER, DEL_FINGER, DELETING_FINGER, CHECK_FINGER, CHECK_NFC}; 

/**
 * Function to manage user's interaction to enter admin password using the keypad
 * @param activeScreen - Screens &
 * @param p - TSPoint
 * @param key_positionX - uint8_t
 * @param passwordKeyboard - String &
 * @param currentIndexPasswd - uint8_t &
 * @param adminPassword - String &
 * @param debounce_ms
 * @param buttonsHomeEnabled[6]
 * @param adminPasswdOK - String&
 */
void detectPressedKeysInKeyboard(Screens &activeScreen,TSPoint p,uint8_t &key_positionX, String &passwordKeyboard, uint8_t &currentIndexPasswd,String &adminPassword, unsigned int debounce_ms, bool buttonsHomeEnabled[6], String&adminPasswdOK);

/**
 * Show notification in mUI displaying the access message to the user.
 * 
 * @param accessMsg - String
 * 
 */
void showAccessMessage(String accessMsg);

/**
 * Show WiFi details like SSID and IP if the device it's connected to Wi-Fi
 * 
 * @param ssid - String &
 * @param ip - String &
 * @param receivedIP - boolean &
 */
void showWiFiDetails(String &ssid, String &ip, boolean &receivedIP);

struct infoSystem
{
   String author;
   String systemVersion;

};

extern Adafruit_GFX_Button initBtn;
#endif