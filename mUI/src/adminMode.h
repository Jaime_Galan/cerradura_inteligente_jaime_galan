/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * Admin Mode
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#ifndef _ADMIN_MODE_
#define _ADMIN_MODE_

#include <Arduino.h>
#include "hardware.h"
#include "funciones.h"

#define BTN_ADMIN_WIDTH 312
#define BTN_ADMIN_HEIGHT 80
//Add FingerPrint Button
#define X_BTN_ADD_FINGER 8
#define Y_BTN_ADD_FINGER 34
#define X_TXT_ADD_FINGER 59
#define Y_TXT_ADD_FINGER 79
//Delete Fingerprint Button
#define X_BTN_DEL_FINGER 8
#define Y_BTN_DEL_FINGER 134
#define X_TXT_DEL_FINGER 38
#define Y_TXT_DEL_FINGER 179
//Check Fingerprint Button
#define X_BTN_CHECK_FINGER 8
#define Y_BTN_CHECK_FINGER 234
#define X_TXT_CHECK_FINGER 40
#define Y_TXT_CHECK_FINGER 279
//Check NFC ID Button
#define X_BTN_CHECK_NFC 8
#define Y_BTN_CHECK_NFC 334
#define X_TXT_CHECK_NFC 75
#define Y_TXT_CHECK_NFC 379
//ADD FINGERPRINT BUTTON KEYBOARD
#define X_BTN_KEYBOARD_FNG 60
#define Y_BTN_KEYBOARD_FNG 220
#define BTN_KEY_FNG_WIDTH  192
#define BTN_KEY_FNG_HEIGHT 80

#define MAX_CAPACITY_STORAGE_FINGERPRINTS 128
#define MAX_DIGITS_FINGER_ID 3
/**
 * Draws the Admin mode's user interface
 */
void showAdminUI();

/**
 * Shows the notification with a accessMessage appropiate depending on the option.
 * @param option - String
 */
void showMessageAccess(String option);

/**
 * Shows NFC ID in hex and decimal value
 * @param uid_nfc - uint8_t []
 * @param uid_length - uint8_t
 */
void show_NFC_ID(uint8_t uid_nfc[], uint8_t uid_length);

/**
 *  Draws the initial AddFinger UI
 */
void drawAddFingerUI();

/**
 *  Draws the intial DeleteFinger UI
 */
void drawDeleteFingerUI();

/**
 *  Shows the finger ID's value in the mode Check Finger ID
 * @param fingerPrintID - int
 */
void showWindowFingerID(int fingerPrintID);

/**
 * Shows the message describing the operations: add Finger or delete finger, depending on the option chosen
 * @param option - String
 */
void drawMessageBox(String option);

/**
 * Writes a number using the keyboard for specified the FingerID
 * @param number - char
 * @param positionX - uint8_t &
 * @param fingerKeyboardID -String &
 * @param currentIndexFingerID - uint8_t &
 * 
 */
void writeFingerIDToPasswdBox(char number,uint8_t &positionX, String &fingerKeyboardID, uint8_t &currentIndexFingerID);

/**
 * Erases last digit typed using the keyboard for entering the desired the FingerID
 * @param positionX
 * @param fingerKeyboardID
 * @param currentIndexFingerID
 */
void eraseFingerIDPasswdBox(uint8_t &positionX, String &fingerKeyboardID, uint8_t &currentIndexFingerID);

/**
 * Function that manages the user interaction with Finger ID's numeric keyboard
 * @param activeScreen - Screens
 * @param p -TSPoint
 * @param key_positionX -uint8_t &
 * @param fingerKeyboardID -String &
 * @param currentIndexFingerID -uint8_t &
 * @param fingerIDSent - int &
 * @param debounce_ms - unsigned int
 * @param isFingerID_OK -String &
 * 
 */
void detectPressedKeysInKeyboardFingerID(Screens &activeScreen,TSPoint p,uint8_t &key_positionX, String &fingerKeyboardID, uint8_t &currentIndexFingerID, int &fingerIDSent, unsigned int debounce_ms,String&isFingerID_OK);

/**
 * Draws the enroll finger UI
 * @param fingerIDSent int &
 */
void drawEnrollFingerUI(int &fingerIDSent);

/**
 * Draws the deleting finger UI
 * @param fingerIDSent -int &
 */
void drawDeletingFingerUI(int &fingerIDSent);

/**
 * Function for enrolling the fingerprint and describes with messsages  vía UI the process.
 * @param fingerIDSent - int &
 * @param fingerPrintsNotMatched - bool &
 */
uint8_t getFingerprintEnroll(int &fingerIDSent,bool &fingerPrintsNotMatched);


#endif
