/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * hardware.cpp
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#include <Arduino.h>
#include "hardware.h"


MCUFRIEND_kbv myGLCD;
TouchScreen touchScn = TouchScreen(XP,YP,XM,YM,300);

PN532_I2C pn532i2c(Wire);
PN532 nfc(pn532i2c);

SoftwareSerial fingerSerial(TX_GREEN,RX_WHITE);
Adafruit_Fingerprint fingerPrnt = Adafruit_Fingerprint(&fingerSerial);

//Serial3 for communicate with ESP8266
// - TX pin 14
// - RX pin 15
//Sleep Controller IL9486
// myGLCD.pushCommand(0x10, NULL, 0);  //Sleep In
// myGLCD.pushCommand(0x11, NULL, 0);  //Sleep Out
