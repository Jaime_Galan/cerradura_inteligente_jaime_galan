/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * Admin Mode
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#include "adminMode.h"

void showAdminUI(){

drawtopBarUI();
drawBackButtonUI();
//Add Fingerprint (Enroll) Button
  myGLCD.fillRoundRect(X_BTN_ADD_FINGER,Y_BTN_ADD_FINGER, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,ORANGE);
  myGLCD.drawRoundRect(X_BTN_ADD_FINGER,Y_BTN_ADD_FINGER, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(X_TXT_ADD_FINGER,Y_TXT_ADD_FINGER);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("ADD FINGERPRINT");
  //Delete Fingerprint Button
  myGLCD.fillRoundRect(X_BTN_DEL_FINGER,Y_BTN_DEL_FINGER, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,ORANGE);
  myGLCD.drawRoundRect(X_BTN_DEL_FINGER,Y_BTN_DEL_FINGER, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(X_TXT_DEL_FINGER,Y_TXT_DEL_FINGER);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("DELETE FINGERPRINT");
  //Check Fingerprint to know fingerID and show it in screen.
  myGLCD.fillRoundRect(X_BTN_CHECK_FINGER,Y_BTN_CHECK_FINGER, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,ORANGE);
  myGLCD.drawRoundRect(X_BTN_CHECK_FINGER,Y_BTN_CHECK_FINGER, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(X_TXT_CHECK_FINGER,Y_TXT_CHECK_FINGER);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("CHECK FINGERPRINT");
  //Check NFC ID to know it and show it in screen.
  myGLCD.fillRoundRect(X_BTN_CHECK_NFC,Y_BTN_CHECK_NFC, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,ORANGE);
  myGLCD.drawRoundRect(X_BTN_CHECK_NFC,Y_BTN_CHECK_NFC, BTN_ADMIN_WIDTH, BTN_ADMIN_HEIGHT,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(X_TXT_CHECK_NFC,Y_TXT_CHECK_NFC);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("CHECK NFC ID");
}

void showMessageAccess(String option){
  
  if(option.equals("GRANTED")){
     //Box Password
    myGLCD.fillRoundRect(20,164, 282, 150,6,SEAGREEN);
    myGLCD.drawRoundRect(20,164, 282, 150,6,TFT_WHITE);
    myGLCD.setFont(&FreeSansBold24pt7b);
    myGLCD.setCursor(60,219);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("ACCESS");
    myGLCD.setCursor(45,279);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("GRANTED");

  }else if(option.equals("DENIED")){

     //Box Password
    myGLCD.fillRoundRect(20,164, 282, 150,6,RED);
    myGLCD.drawRoundRect(20,164, 282, 150,6,TFT_WHITE);
    myGLCD.setFont(&FreeSansBold24pt7b);
    myGLCD.setCursor(60,219);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("ACCESS");
    myGLCD.setCursor(67,279);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("DENIED");

  }else if(option.equals("FIN_ID_OK")){
     //Box Password
    myGLCD.fillRoundRect(20,164, 282, 150,6,SEAGREEN);
    myGLCD.drawRoundRect(20,164, 282, 150,6,TFT_WHITE);
    myGLCD.setFont(&FreeSansBold24pt7b);
    myGLCD.setCursor(140,219);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("ID");
    myGLCD.setCursor(125,279);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("OK");

  }else if(option.equals("FIN_ID_NOK")){
     //Box Password
    myGLCD.fillRoundRect(20,164, 282, 150,6,RED);
    myGLCD.drawRoundRect(20,164, 282, 150,6,TFT_WHITE);
    myGLCD.setFont(&FreeSansBold24pt7b);
    myGLCD.setCursor(140,219);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("ID");
    myGLCD.setCursor(70,279);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("WRONG");


  }
  
}

void show_NFC_ID(uint8_t uid_nfc[], uint8_t uid_length){

    String uid_aux = (char *) uid_nfc;
    char uid_hex_NFC[(uid_length * 2) + 1];
    char * ptr = &uid_hex_NFC[0];

  myGLCD.fillRoundRect(20,234, 282, 160,6,SEAGREEN);
  myGLCD.drawRoundRect(20,234, 282, 160,6,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(125,269);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("NFC ID");
  
  myGLCD.setCursor(110,319);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  for(uint8_t i = 0; i<uid_length;i++){
     //uid_NFC_aux[i] = uid_nfc[i];
     /* sprintf converts each byte to 2 chars hex string and a null byte, for example
     * 10 => "0A\0".
     *
     * These three chars would be added to the output array starting from
     * the ptr location, for example if ptr is pointing at 0 index then the hex chars
     * "0A\0" would be written as output[0] = '0', output[1] = 'A' and output[2] = '\0'.
     *
     * sprintf returns the number of chars written execluding the null byte, in our case
     * this would be 2. Then we move the ptr location two steps ahead so that the next
     * hex char would be written just after this one and overriding this one's null byte.
     *
     * We don't need to add a terminating null byte because it's already added from 
     * the last hex string. Answered by user razzak in StackOverFlow
     * https://stackoverflow.com/questions/6357031/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-in-c */  

     ptr += sprintf(ptr,"%X", uid_nfc[i]);  
  }
  myGLCD.print(uid_hex_NFC);
  myGLCD.setCursor(90,369);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  for(uint8_t i = 0; i<uid_length;i++){
    myGLCD.print(uid_nfc[i]);
    myGLCD.print(" ");
  }
  
}

void drawAddFingerUI(){
  drawtopBarUI();
  drawBackButtonUI();
  drawMessageBox("addFinger");
   //BUTTON KEYBOARD
  myGLCD.fillRoundRect(X_BTN_KEYBOARD_FNG,Y_BTN_KEYBOARD_FNG, BTN_KEY_FNG_WIDTH, BTN_KEY_FNG_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_KEYBOARD_FNG,Y_BTN_KEYBOARD_FNG, BTN_KEY_FNG_WIDTH, BTN_KEY_FNG_HEIGHT,BUTTONS_HOME_RADIUS,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(90,270);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("KEYBOARD");

}
void drawMessageBox(String option){
    
    if(option.equals("addFinger")){
      myGLCD.fillRoundRect(20,40, 282, 130,6,BLUE);
      myGLCD.drawRoundRect(20,40, 282, 130,6,TFT_WHITE);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(45,70);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Ready to enroll a fingerprint.");
      myGLCD.setCursor(45,100);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Please type in the ID#");
      myGLCD.setCursor(45,130);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("(from 1 to 127) you want");
      myGLCD.setCursor(45,160);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("to save this finger as...");

    } else if(option.equals("deleteFinger")){
      myGLCD.fillRoundRect(20,40, 282, 130,6,BLUE);
      myGLCD.drawRoundRect(20,40, 282, 130,6,TFT_WHITE);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(45,70);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Ready to delete a fingerprint.");
      myGLCD.setCursor(45,100);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Please type in the ID#");
      myGLCD.setCursor(45,130);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("(from 1 to 127) you want");
      myGLCD.setCursor(45,160);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("to delete this finger as...");
 

    }
}
void drawDeleteFingerUI(){
  drawtopBarUI();
  drawBackButtonUI();
  drawMessageBox("deleteFinger");

   //BUTTON KEYBOARD
  myGLCD.fillRoundRect(X_BTN_KEYBOARD_FNG,Y_BTN_KEYBOARD_FNG, BTN_KEY_FNG_WIDTH, BTN_KEY_FNG_HEIGHT,BUTTONS_HOME_RADIUS,BLUE);
  myGLCD.drawRoundRect(X_BTN_KEYBOARD_FNG,Y_BTN_KEYBOARD_FNG, BTN_KEY_FNG_WIDTH, BTN_KEY_FNG_HEIGHT,BUTTONS_HOME_RADIUS,TFT_WHITE);
  myGLCD.setFont(&FreeSans12pt7b);
  myGLCD.setCursor(90,270);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("KEYBOARD");

}

void showWindowFingerID(int fingerPrintID){

    myGLCD.fillRoundRect(20,164, 282, 150,6,SEAGREEN);
    myGLCD.drawRoundRect(20,164, 282, 150,6,TFT_WHITE);
    myGLCD.setFont(&FreeSansBold24pt7b);
    myGLCD.setCursor(60,219);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("FingerID");
    myGLCD.drawLine(50,235,260,235,TFT_WHITE);
    myGLCD.setCursor(150,279);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print(fingerPrintID);

}

void writeFingerIDToPasswdBox(char number,uint8_t &positionX, String &fingerKeyboardID, uint8_t &currentIndexFingerID){
  
  if(fingerKeyboardID.length()<MAX_DIGITS_FINGER_ID){
    fingerKeyboardID.concat(number);
  
    if(currentIndexFingerID>=0 && currentIndexFingerID<MAX_DIGITS_FINGER_ID){
      myGLCD.setFont(&FreeSansBold24pt7b);
      myGLCD.setCursor(positionX,94);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("*");
      Serial.println(currentIndexFingerID);
      positionX+=26;
      if(currentIndexFingerID!=2){
            currentIndexFingerID++;
      }
    }
  }
#if DEBUG
  Serial.print("FingerID:");
  Serial.println(fingerKeyboardID);
#endif
}

void eraseFingerIDPasswdBox(uint8_t &positionX, String &fingerKeyboardID, uint8_t &currentIndexFingerID){
   if(fingerKeyboardID.length()<=MAX_DIGITS_PASSWD){
      if(currentIndexFingerID>=0 && currentIndexFingerID<MAX_DIGITS_FINGER_ID){

          if(positionX>20)
            positionX-=26;
          else
            positionX = 20; 

          myGLCD.setFont(&FreeSansBold24pt7b);
          myGLCD.setCursor(positionX,94);
          myGLCD.setTextColor(TFT_DARKGREY);
          myGLCD.setTextSize(1);
          myGLCD.print("*");
          if(currentIndexFingerID!=0)
            currentIndexFingerID--;
          fingerKeyboardID.remove(currentIndexFingerID);
          }
#if DEBUG
              Serial.print("Finger_C:");
              Serial.println(fingerKeyboardID); 
#endif
  }

}

void detectPressedKeysInKeyboardFingerID(Screens &activeScreen,TSPoint p,uint8_t &key_positionX, String &fingerKeyboardID, uint8_t &currentIndexFingerID, int &fingerIDSent, unsigned int debounce_ms,String&isFingerID_OK){
  
   if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('1', key_positionX, fingerKeyboardID, currentIndexFingerID);
         
          activeScreen = FINGER_KEYPAD;
        }
      if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('2', key_positionX, fingerKeyboardID, currentIndexFingerID);
         
          activeScreen = FINGER_KEYPAD;
        }
      if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_1 && p.y < (Y_BTNS_KEYS_ROW_1+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('3', key_positionX, fingerKeyboardID, currentIndexFingerID);
          
          activeScreen = FINGER_KEYPAD;
        }
      if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('4', key_positionX, fingerKeyboardID, currentIndexFingerID);
          
          activeScreen = FINGER_KEYPAD;
        }
      if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('5', key_positionX, fingerKeyboardID, currentIndexFingerID);
          
          activeScreen = FINGER_KEYPAD;
        }
      if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_2 && p.y < (Y_BTNS_KEYS_ROW_2+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('6', key_positionX, fingerKeyboardID, currentIndexFingerID);
         
          activeScreen = FINGER_KEYPAD;
        }
        if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('7', key_positionX, fingerKeyboardID, currentIndexFingerID);
          
          activeScreen = FINGER_KEYPAD;
        }
        if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('8', key_positionX, fingerKeyboardID, currentIndexFingerID);
         
          activeScreen = FINGER_KEYPAD;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_3 && p.y < (Y_BTNS_KEYS_ROW_3+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('9', key_positionX, fingerKeyboardID, currentIndexFingerID);
          
          activeScreen = FINGER_KEYPAD;
        }
         if(p.x>X_BTNS_KEYS_COL_1 && p.x < (X_BTNS_KEYS_COL_1+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          //Clear
           eraseFingerIDPasswdBox(key_positionX, fingerKeyboardID, currentIndexFingerID);
          activeScreen = FINGER_KEYPAD;
        }
        if(p.x>X_BTNS_KEYS_COL_2 && p.x < (X_BTNS_KEYS_COL_2+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          writeFingerIDToPasswdBox('0', key_positionX, fingerKeyboardID, currentIndexFingerID);
          
          activeScreen = FINGER_KEYPAD;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTONS_KEYPAD_WIDTH) && p.y > Y_BTNS_KEYS_ROW_4 && p.y < (Y_BTNS_KEYS_ROW_4+BUTTONS_KEYPAD_HEIGHT)){
           delayKeyboard_ms(debounce_ms);
           //Check password (Send Button)
           fingerPrnt.getTemplateCount();
           uint8_t capacityStorageFingers = MAX_CAPACITY_STORAGE_FINGERPRINTS - fingerPrnt.templateCount;
           Serial.print(fingerPrnt.templateCount);
           if(fingerKeyboardID.length() < 4 && capacityStorageFingers > 0 && capacityStorageFingers < 128){
             
             int fingerKeyID = atoi(fingerKeyboardID.c_str());
             Serial.print("ID:");
             Serial.print(fingerKeyID);
             if(fingerKeyID != 0 && fingerKeyID > 1 && fingerKeyID < 128){ //Cannot delete first FingerPrint
              fingerIDSent = fingerKeyID;
              fingerKeyboardID=""; //Reset passwd for a new entry by the user next time.
              currentIndexFingerID = 0;
              key_positionX = 20; 
              isFingerID_OK = "TRUE";

             }else if(fingerKeyID == 0  || fingerKeyID == 1 || fingerKeyID > 127){
              fingerKeyboardID=""; //Reset passwd for a new entry by the user next time.
              currentIndexFingerID = 0;
              key_positionX = 20;
              isFingerID_OK = "FALSE";
            }
              
           }
          activeScreen = FINGER_KEYPAD;
        }
        if(p.x>X_BTNS_KEYS_COL_3 && p.x < (X_BTNS_KEYS_COL_3+BUTTON_BACK_WIDTH) && p.y > Y_BUTTON_BACK_KEYBOARD && p.y < (Y_BUTTON_BACK_KEYBOARD+BUTTON_BACK_HEIGHT)){
          delayKeyboard_ms(debounce_ms);
          //Back Button
          myGLCD.fillScreen(TFT_BLACK);
          drawAddFingerUI();
          fingerKeyboardID=""; //Reset passwd for a new entry by the user next time.
          currentIndexFingerID = 0;
          key_positionX = 20;
          activeScreen = ADD_FINGER;

        }

}

void drawEnrollFingerUI(int &fingerIDSent){
  drawtopBarUI();
  myGLCD.fillRoundRect(20, 40, 282, 60,6,BLUE);
  myGLCD.drawRoundRect(20, 40, 282, 60,6,TFT_WHITE);
  myGLCD.setCursor(30,70);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Enrolling Finger ID: ");
  myGLCD.print(fingerIDSent);
  myGLCD.fillRoundRect(20, 120, 282, 280,6,BLUE);
  myGLCD.drawRoundRect(20, 120, 282, 280,6,TFT_WHITE);
  
}
void drawDeletingFingerUI(int &fingerIDSent){
  drawtopBarUI();
  myGLCD.fillRoundRect(20, 40, 286, 60,6,BLUE);
  myGLCD.drawRoundRect(20, 40, 286, 60,6,TFT_WHITE);
  myGLCD.setCursor(30,70);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Deleting Finger ID: ");
  myGLCD.print(fingerIDSent);
  myGLCD.fillRoundRect(20, 120, 286, 100,6,BLUE);
  myGLCD.drawRoundRect(20, 120, 286, 100,6,TFT_WHITE);
  
}
uint8_t getFingerprintEnroll(int &fingerIDSent,bool &fingerPrintsNotMatched) {

  int p = -1;
  Serial.print("Waiting for valid finger to enroll as #"); Serial.println(fingerIDSent);
 
  while (p != FINGERPRINT_OK) {
    p = fingerPrnt.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,140);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.println(".");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,140);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,140);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,140);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,140);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Unknown error");
      break;
    }
  }

  // OK success!

  p = fingerPrnt.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,170);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Image converted");
     
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,170);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,170);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,170);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,170);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Could not find fingerprint features");
      
      return p;
    default:
      Serial.println("Unknown error");
      delayKeyboard_ms(20);
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,170);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("unknown error");
      return p;
  }
  
  Serial.println("Remove finger");
   myGLCD.setFont(&FreeSans9pt7b);
   myGLCD.setCursor(30,200);
   myGLCD.setTextColor(WHITE);
   myGLCD.setTextSize(1);
   myGLCD.print("Remove finger");
  unsigned long timer = millis();
  while(millis()<timer+2000){}
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = fingerPrnt.getImage();
  }
  Serial.print("ID "); Serial.println(fingerIDSent);
  p = -1;
  Serial.println("Place same finger again");
   myGLCD.setFont(&FreeSans9pt7b);
   myGLCD.setCursor(30,230);
   myGLCD.setTextColor(WHITE);
   myGLCD.setTextSize(1);
   myGLCD.print("Place same finger again");
  while (p != FINGERPRINT_OK) {
    p = fingerPrnt.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,260);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,260);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,260);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,260);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,260);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Unknown error");
      break;
    }
  }

  // OK success!

  p = fingerPrnt.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,290);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,290);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,290);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,290);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,290);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Could not find fingerprint features");
      
      return p;
    default:
      Serial.println("Unknown error");
      myGLCD.setFont(&FreeSans9pt7b);
      myGLCD.setCursor(30,290);
      myGLCD.setTextColor(WHITE);
      myGLCD.setTextSize(1);
      myGLCD.print("Unknown error");
      return p;
  }
  
  // OK converted!
  Serial.print("Creating model for #");  Serial.println(fingerIDSent);
  myGLCD.setFont(&FreeSans9pt7b);
  myGLCD.setCursor(30,320);
  myGLCD.setTextColor(WHITE);
  myGLCD.setTextSize(1);
  myGLCD.print("Creating model for id");
  p = fingerPrnt.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("Prints matched!");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,350);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Prints matched!");
    fingerPrintsNotMatched = false;
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,350);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Communication error");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    Serial.println("Fingerprints did not match");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,350);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Fingerprints did not match");
    fingerPrintsNotMatched = true;
    return p;
  } else {
    Serial.println("Unknown error");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,350);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Unknown error");
    return p;
  }   
  
  Serial.print("ID "); Serial.println(fingerIDSent);
  p = fingerPrnt.storeModel(fingerIDSent);
  if (p == FINGERPRINT_OK) {
    Serial.println("Stored!");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,380);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Stored FingerPrint ID:");
    myGLCD.print(fingerIDSent);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,380);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not store in that location");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,380);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,380);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Error writing to flash");
    return p;
  } else {
    Serial.println("Unknown error");
    myGLCD.setFont(&FreeSans9pt7b);
    myGLCD.setCursor(30,380);
    myGLCD.setTextColor(WHITE);
    myGLCD.setTextSize(1);
    myGLCD.print("Unknown error");
    return p;
  }   
}


