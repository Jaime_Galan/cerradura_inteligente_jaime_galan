/*
 * mUI - Module User Interface (modulo Interfaz de Usuario)
 * 
 * hardware.h
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#ifndef _HARDWARE_H
#define _HARDWARE_H

#include <MCUFRIEND_kbv.h>        //use GLUE class and constructor
#include <SPI.h>
#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_Fingerprint.h>
#include "TouchScreen.h"

// Pinout LCD DISPLAY
#define LCD_RST A4 
#define LCD_CS  A3 
#define LCD_RS  A2 
#define LCD_WR  A1 
#define LCD_RD  A0 
#define MODEL 0x9486

//TouchScreen
//Pins for manage the display
#define XP 9   //Pin de control digital
#define YP A2  //Pin análogo de control
#define XM A3  //Pin análogo de control
#define YM 8   //Pin digital de control

#define TS_MINX 130
#define TS_MAXX 905

#define TS_MINY 75
#define TS_MAXY 930

// Definir los niveles de presión mínima y máxima
#define MINPRESSURE 10
#define MAXPRESSURE 1000

//Definir los valores para calibrar la pantalla táctil
#define X_STPT  75    // Valor de X inicial
#define X_ENPT  905    // Valor de X final
#define Y_STPT  145    // Valor de Y inicial
#define Y_ENPT  880    // Valor de Y final

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

//PinOut FingerPrint Sensor
#define TX_GREEN 51
#define RX_WHITE 53
extern MCUFRIEND_kbv myGLCD;
extern TouchScreen touchScn;
extern PN532 nfc;
extern SoftwareSerial fingerSerial;
extern Adafruit_Fingerprint fingerPrnt;
//#define NUMSAMPLES 3 definido en TouchScreen.cpp

#endif