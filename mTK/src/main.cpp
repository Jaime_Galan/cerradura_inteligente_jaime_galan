/*
 * mTK - Module Turn Key (módulo Girar LLave)
 * 
 *  Si al arrancar el sistema, transcurre un minuto sin recibir ninguna orden,
 *  ya sea mediante el interruptor o de la RPI vía MQTT, el ESP32 entra en el modo de bajo consumo
 *  deep-sleep durante 10 segundos. Transcurridos los diez segundos, el ESP32 se despierta durante un minuto,
 *  volviendo al modo normal de funcionamiento, si en este caso recibe una orden mediante MQTT,
 *  efectúa la orden de abrir la cerradura, después de 20 segundos cierra la puerta automáticamente,
 *  se desconecta del Wi-Fi y se va a dormir durante 10 segundos.
 *  Si la orden fue recibida mediante el interruptor, en función del estado de la cerradura,
 *  abre o cierra la puerta, se desconecta del Wi-Fi y entra en el modo deep-sleep durante 10 segundos.
 *  Además siempre que hay un cambio en el estado de la cerradura, se actualiza el estado en la memoria flash del sistema.
 * 
 *  If, when the system is started, one minute passes without receiving any order, either from the button
 *  or from the RPI over MQTT, the ESP32 enters the deep-sleep mode for 10 seconds. After 10s, the ESP32 wakes
 *  up for one minute, returning to normal operation mode. If in this case it receives an order via MQTT,
 *  it proceeds to open the lock, after 20s it closes the door automatically, disconnects from Wi-Fi and goes
 *  to sleep for 10s. If the order was received through the button, depending on the state of the lock,
 *  it opens or closes the door, disconnects from the Wi-Fi and enters the deep-sleep mode for 10s.
 *  Furthermore, whenever there is a change in lock status, the lock status is updated in the system's flash memory.
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/

#include <Arduino.h>
#include "hardware.h"
#include <WiFi.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <esp_wifi.h>

#define uS_TO_S_FACTOR 1000000
#define TIME_TO_SLEEP  10 //10s
#define EEPROM_SIZE 1 //1 Byte to access in Flash Memory
#define ADDR_STATEDOOR 0
#define T_20S 20000

//Using deep sleep mode with Arduino Framework your sketch only runs the setup section and never runs the loop() part.
RTC_DATA_ATTR const char* ssid     = "";
RTC_DATA_ATTR const char* password = "";

//MQTT BROKER IP - Raspberry Pi 3
RTC_DATA_ATTR const char* mqtt_server = "192.168.1.17";
RTC_DATA_ATTR const char* mqtt_user = "openhabian";
RTC_DATA_ATTR const char* mqtt_connectionPasswd = "openhabian!";
//mqtt port: TCP/IP: 1883
//RTC_DATA_ATTR attribute for store in RTC Memory the variables
RTC_DATA_ATTR static const int servoPin = 23;
RTC_DATA_ATTR static const int buttonPin = 27;
RTC_DATA_ATTR volatile bool isDoorInterruptBtn = false;
RTC_DATA_ATTR int positionServo = 90;
RTC_DATA_ATTR enum States_mTK{IDLE, OPEN, CLOSE};
RTC_DATA_ATTR States_mTK state;

RTC_DATA_ATTR unsigned int bootCount = 0;

RTC_DATA_ATTR WiFiClient esp32Client;
RTC_DATA_ATTR PubSubClient client(esp32Client);
RTC_DATA_ATTR long lastReconnectAttempt = 0;

RTC_DATA_ATTR bool isDoorOpenedMQTT = false;
RTC_DATA_ATTR bool isDoorOpened = false;

bool openDoor();
bool closeDoor();
boolean reconnect_MQTTClient(void);

/* void IRAM_ATTR isr_button():
*  Interrupt Service Routine
*  Activates a flag to indicated that the button was pressed. 
*/
void IRAM_ATTR isr_button(){ //IRAM_ATTR tells the compiler, that this code Must always be in the 
// ESP32's IRAM, the limited 128k IRAM.  use it sparingly. https://github.com/espressif/arduino-esp32/issues/855
// is used to run the interrupt code in RAM, otherwise code is stored in flash and it’s slower
 isDoorInterruptBtn = true;
}

//Function executed when a message MQTT is received
void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
  //payload[length] = '\0';
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

 if(strncmp((char *)payload, "openDoor",strlen("openDoor")) == 0){
    isDoorOpenedMQTT = true;
    state = OPEN;
 }
 
}

void setup() {
 
  Serial.begin(115200);
  EEPROM.begin(EEPROM_SIZE);
  isDoorOpened = EEPROM.read(ADDR_STATEDOOR);
  pinMode(buttonPin, INPUT); 
  attachInterrupt(digitalPinToInterrupt(buttonPin), isr_button, RISING);
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.print("Hello.Setup ESP32 to deep sleep " + String(TIME_TO_SLEEP) + " seconds");
  WiFi.begin(ssid,password);

  while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: "); 
    Serial.println(WiFi.localIP());
    
    client.setServer(mqtt_server,1883);
    client.setCallback(callback);
    client.connect("mTKClient",mqtt_user, mqtt_connectionPasswd);
    Serial.println("MQTT connected");
    if(client.subscribe("home/mainDoor/state")){ 
      Serial.println("Subscribed to state");
    }
    servomotor.attach(servoPin,Servo::CHANNEL_NOT_ATTACHED,0,180);
    unsigned long initSystem = millis();
   if(bootCount == 0){
     bootCount++;
     state = IDLE;

   }else{
     bootCount++;
     
     Serial.println("Boot " + String(bootCount) + ".System State: " + String(state));
     Serial.println("States Allowed: 0 - IDLE, 1 - OPEN, 2 - CLOSE");
     
  
   }

   while(1)
   {
     switch (state)
    {
      case IDLE:
        if(!client.connected()){
          long now = millis();
          if (now - lastReconnectAttempt > 3000) {
            lastReconnectAttempt = now;
            // Attempt to reconnect
            if (reconnect_MQTTClient()) {
              lastReconnectAttempt = 0;
            }
          }
      
        }else{
          client.loop();
          unsigned long now = millis(); 
        }
        if(isDoorInterruptBtn){
            unsigned long now = millis();
            isDoorInterruptBtn = false;
            while(millis() < now + 100){}; //after 100 ms starts
            if(isDoorOpened){
              if(closeDoor()){
                isDoorOpened = false;
                EEPROM.write(ADDR_STATEDOOR,isDoorOpened);
                EEPROM.commit();
                esp_wifi_stop();
                Serial.println("ESP32 goes to deep sleep");
                esp_deep_sleep_start();
              }
          
            }else{
              if(openDoor()){
                isDoorOpened = true;
                EEPROM.write(ADDR_STATEDOOR,isDoorOpened);
                EEPROM.commit();
                esp_wifi_stop();
                Serial.println("ESP32 goes to deep sleep");
                esp_deep_sleep_start();
              }

            } 
        
        } else if(millis() > (initSystem + 60000) && !isDoorOpenedMQTT){
           esp_wifi_stop();
           Serial.println("ESP32 goes to deep sleep");
           esp_deep_sleep_start();

        }
        
    
  
        break;

        case CLOSE:

        if(isDoorOpenedMQTT){
          if(closeDoor()){
            isDoorOpenedMQTT = false;
            isDoorOpened = false;
            EEPROM.write(ADDR_STATEDOOR,isDoorOpened);
            EEPROM.commit();
            state = IDLE;
            esp_wifi_stop();
            Serial.println("ESP32 goes to deep sleep");
            esp_deep_sleep_start();
          }
      
        }

        break;

        case OPEN:
     
        if(isDoorOpenedMQTT){
          openDoor();
          unsigned long timerCloseDoor = millis();
          while(millis()<timerCloseDoor+T_20S){};
          isDoorOpened = true;
          EEPROM.write(ADDR_STATEDOOR,isDoorOpened);
          EEPROM.commit();
          state = CLOSE;
        }

        break;
  
    }

  }

}

void loop(){
}

/* Function  bool openDoor()
   Open the door and returns a boolean indicating the success or not for opening the lock
*/
bool openDoor(){
  bool isDoorOpened = false;
  servomotor.attach(servoPin,Servo::CHANNEL_NOT_ATTACHED,0,180);
  unsigned long openTime = millis();

 while (millis() < openTime + 5500)
 {
   for (positionServo = 90; positionServo <= 270; positionServo += 1) { // goes from 90 degrees to 270 degrees
    // in steps of 1 degree
    servomotor.write(positionServo); // tell servo to go to position in variable 'pos'
    unsigned long timer = millis();
    while(millis()<timer+20){} // waits 20ms for the servo to reach the position
    Serial.println(positionServo);
    
  }
   
 }
 servomotor.detach(); 
 isDoorOpened = true;

 return isDoorOpened;
}

/* Function  bool closeDoor()
   Close the door and returns a boolean indicating the success or not for closing the lock
*/
bool closeDoor(){
 bool isDoorClosed = false;
 servomotor.attach(servoPin,Servo::CHANNEL_NOT_ATTACHED,180,0);
 unsigned long closeTime = millis();
 
 while (millis() < closeTime + 5500)
 {
   for (positionServo = 90; positionServo >= -90; positionServo -= 1) { // goes from 90 degrees to -90 degrees
    // in steps of 1 degree
    servomotor.write(positionServo);              // tell servo to go to position in variable 'pos'
    unsigned long timer = millis();
    while(millis()<timer+20){} // waits 20ms for the servo to reach the position
    Serial.println(positionServo);
    
  }
   
 }
 servomotor.detach();
 isDoorClosed = true;

 return isDoorClosed;
}

/* Function reconnect_MQTTClient(void)
 * Reconnect the MQTT Client with the MQTT Server and subscribes
 * to the topic "home/mainDoor/state"
*/
boolean reconnect_MQTTClient(void){
  long lastSubscribeAttempt;
  Serial.print("Attempting MQTT connection...");
  // Attempt to connect
  if (client.connect("mTKClient",mqtt_user, mqtt_connectionPasswd)) {
      
    Serial.println("MQTT connected");
    long now = millis();
    if (now - lastSubscribeAttempt > 2000){
      lastSubscribeAttempt = now;
      //Attempt to reconnect
      if(client.subscribe("home/mainDoor/state")) {
        lastSubscribeAttempt = 0;
      }
    }
  }
  return client.connected();
}