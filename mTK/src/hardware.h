/*
 * mTK - Module Turn Key (módulo Girar LLave)
 * 
 * Author: Jaime Galan Martinez
 * Version: v1.0
*/
#ifndef _HARDWARE_mTK_H
#define _HARDWARE_mTK_H

#include <Arduino.h>
#include <Servo.h>

extern Servo servomotor; //SM-S4315R

#endif