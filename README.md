# Cerradura_Inteligente_Jaime_Galan

Proyecto Fin de Grado | Bachelor's Thesis

**Diseño de una cerradura inteligente e integración en openHAB** 

**(Design of Smart Lock System for Smart Home powered by the openHAB platform)**

Author: **Jaime Galán Martínez**

This project consists of designing a smartlock system powered by openHAB platform, since it is
not desired that the security of the system depends on the cloud. 

Thanks to the advance and generalization of IoT (Internet of Things) access control technologies
are available as well as many devices for most of the society. Therefore, as we have this 
technology, it is time to apply it to homes so that they can have innovative access methods such
as the smartphone, fingerprint, wireless cards or keyboard to access and control the door of their home.

Furthermore, it is done from a DIY (Do It Yourself) point of view to encourage personal creativity
in the technological field to develop a solution of lower material cost than commercial solutions,
using open source software and hardware. In addition, wireless technologies will be used so that
the system can be placed anywhere without the need to do any work.

The developed system is composed of three modules: mUI, mControl and mTK. The mUI is in
charge of the user’s interaction with the system, so that he can be identified by a keyboard key,
his fingerprint or by an NFC card. This data is transmitted to the mControl, which will manage
the system and, if necessary, sends the order to the mTK to open the lock.A wooden mock-up
has also been made, in which the lock has been stuffed to carry out the verification of the
opening and closing the lock. 

After analyzing the results obtained from the prototype made, it has been concluded that it
complies with the specifications described, since the lock is capable of being opened by means
of a numeric keypad, NFC cards or keys and fingerprints. It should be noted that, depending on
the key used, it identifies the user who wants to access and, if it complies with the date and time
restrictions, it is granted access to the home by showing a notification on the touchscreen of the mUI. 

